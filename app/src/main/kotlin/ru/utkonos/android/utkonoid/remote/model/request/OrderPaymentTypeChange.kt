package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderPaymentTypeChange(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("PaymentTypeId") var PaymentTypeId:String,
   @SerializedName("PaymentSuccessUrl") var PaymentSuccessUrl:String,
   @SerializedName("PaymentCancelUrl") var PaymentCancelUrl:String,
   @SerializedName("AndroidPay") var AndroidPay:String,
   @SerializedName("ApplePay") var ApplePay:String,
   @SerializedName("SamsungPay") var SamsungPay:String,
   @SerializedName("GooglePay") var GooglePay:String
)