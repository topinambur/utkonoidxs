package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PhoneConfirmRequestAddRequest(
    @SerializedName("Phone") var Phone:String
)
