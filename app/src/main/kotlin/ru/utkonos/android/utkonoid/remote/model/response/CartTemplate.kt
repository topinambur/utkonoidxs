package ru.utkonos.android.utkonoid.remote.model.response

data class CartTemplate(
    var Id: String,
    var Name: String,
    var Count: Int,
    var Price: Double,
    var LastUse: String
)