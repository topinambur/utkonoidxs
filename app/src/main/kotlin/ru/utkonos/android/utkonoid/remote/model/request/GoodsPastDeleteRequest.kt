package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsPastDeleteRequest(
   @SerializedName("GoodsItemId") var GoodsItemId:String
)