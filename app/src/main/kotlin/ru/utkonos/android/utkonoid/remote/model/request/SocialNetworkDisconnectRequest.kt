package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SocialNetworkDisconnectRequest(
    @SerializedName("SocialNetworkId") var SocialNetworkId:String
)