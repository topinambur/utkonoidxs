package ru.utkonos.android.utkonoid.data.source

import ru.utkonos.android.utkonoid.data.repository.UtkonosDataStore
import javax.inject.Inject

/**
 * Create an instance of a UtkonosDataStore
 */
open class UtkonosDataStoreFactory @Inject constructor( private val utkonosRemoteDataStore: UtkonosRemoteDataStore ) {

    /**
     * Returns a DataStore based on whether or not there is content in the cache and the cache
     * has not expired
     */
    open fun retrieveDataStore(): UtkonosDataStore {
        //if (utkonosCache.isCached() && !utkonosCache.isExpired()) {
        //    return retrieveCacheDataStore()
        //}
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveRemoteDataStore(): UtkonosDataStore {
        return utkonosRemoteDataStore
    }

}