package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class Cart (
        /**
         * Стоимость без скидки
         */
        @SerializedName("BaseTotalCost") var BaseTotalCost: String,

        /**
         * Минимально возможное кол-во баллов, которое возможно использовать. Определяется материалами, для которых обязательна оплата бонусами.
         */
        @SerializedName("BonusAvailableMin") var BonusAvailableMin: Int,

        /**
         * Ограничение на минимальное использование бонусных баллов для заказа. Зависит от бонусной группы покупателя.
         */
        @SerializedName("BonusMinConsumption") var BonusMinConsumption: Int = 0,

        /**
         * Расчетное (на основе содержимого корзины) количество бонусных баллов, которое можно применить
         */
        @SerializedName("BonusAvailableMax") var BonusAvailableMax: Int = 0,

        /**
         * Реальное максимальное количество бонусных баллов с учетом состояния счета и использования на текущую корзину
         */
        @SerializedName("BonusAvailableMaxReal") var BonusAvailableMaxReal: Int,

        /**
         * Интервал доставки
         */
        @SerializedName("DeliveryIntervalId") var DeliveryIntervalId: String,

        /**
         * Время до закрытия выбранного интервала
         */
        @SerializedName("DeliveryIntervalCutoff") var DeliveryIntervalCutoff:String,

        /**
         * 	Тип доставки
         */
        @SerializedName("DeliveryTypeId") var DeliveryTypeId:Int,

        /**
         * Стоимость доставки
         */
        @SerializedName("DeliveryPrice") var DeliveryPrice: String,

        /**
         * Размер скидки по промокоду
         */
        @SerializedName("DiscountValue") var DiscountValue: String,


        /**
         * Выгода по акциям, скидкам и промокоду (суммарно)
         */
        @SerializedName("PromocodeDiscountValue") var PromocodeDiscountValue: Double,

        /**
         * 	Код ошибки
         */
        @SerializedName("ErrorCode") var ErrorCode : String,

        /**
         * Предоставлена ли пользователю бесплатная доставка
         */
        @SerializedName("FreeDelivery") var FreeDelivery:Int,

        /**
         * Идентификатор корзины (может не совпадать с UserId)
         */
        @SerializedName("Id") var Id: String,

        /**
         * Магазин доставки (deprecated)
         */
        @SerializedName("PavilionId")	var PavilionId:String,


        @SerializedName("PositionsCount") var PositionsCount: Int = 0,

        /**
         * выгода по акциям и скидкам(суммарно)
         */
        @SerializedName("Saving") var Saving: String? = null,

        /**
         * Общая масса
         */
        @SerializedName("TotalWeight") var TotalWeight: String,

        /**
         * Общий объем
         */
        @SerializedName("TotalVolume")  var TotalVolume:Int,

        /**
         * Общая стоимость
         */
        @SerializedName("TotalCost") var TotalCost: String,

        /**
         * Идентификатор последнего адреса доставки
         */
        @SerializedName("UserAddressId") var UserAddressId: String,

        /**
         * Идентификатор пользователя
         */
        @SerializedName("UserId") var UserId:String,

        /**
         * Содержимое корзины
         */
        @SerializedName("Goods") var Goods: ArrayList<CartItem>,

        /**
         * Использованные промокоды
         */
        @SerializedName("CartPromocodeList") var CartPromocodeList: ArrayList<CartPromocode>,

        /**
         * Список акций промо-кодов, примененных на текущую корзину или заказ
         */
        @SerializedName("PromocodeActionList") var PromocodeActionList: ArrayList<PromocodeAction>,

        /**
         * Информация по использованию бонусных баллов
         */
        @SerializedName("BonusUsing") var BonusUsing: BonusCartUsing,

        /**
         * Информационных сообщения и ошибки корзины
         */
        @SerializedName("CartNotices") var CartNotices: CartNotices,

        /**
         * Время начала интервала доставки
         */
        @SerializedName("DeliveryIntervalBegin") var DeliveryIntervalBegin: String,

        /**
         * Время окончания интервала доставки
         */
        @SerializedName("DeliveryIntervalEnd") var DeliveryIntervalEnd: String,

        /**
         * Адрес доставки
         */
        @SerializedName("UserAddress") var UserAddress: JsonElement,

        /**
         * Название интервала доставки
         */
        @SerializedName("DeliveryIntervalName") var DeliveryIntervalName: String,

        /**
         *
         */
        @SerializedName("FoodboxDelivery") var FoodboxDelivery: FoodboxDelivery,

        /**
         * Примечание покупателя к заказу (если есть). См OrderBuyerNoteSaveOrder, OrderBuyerNoteSaveCart
         */
        @SerializedName("OrderBuyerNote") var OrderBuyerNote: String,

        @SerializedName("Delivery") var Delivery: Delivery,

        @SerializedName("GoodsItemList") var GoodsItemList: ArrayList<GoodsItem>
)