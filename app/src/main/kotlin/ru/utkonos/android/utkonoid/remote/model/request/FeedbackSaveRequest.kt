package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FeedbackSaveRequest(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("Feedback") var Feedback:String,
   @SerializedName("OrderItemProblem") var OrderItemProblem:String,
   @SerializedName("Comment") var Comment:String,
   @SerializedName("IWill") var IWill:String,
   @SerializedName("IRecommend") var IRecommend:String,
   @SerializedName("PreferContact") var PreferContact:String
)