package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FcmTokenSaveRequest(
   @SerializedName("FcmToken") var FcmToken:String
)