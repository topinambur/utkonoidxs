package ru.utkonos.android.utkonoid.remote.model.response

import java.util.ArrayList

data class WishListGoodsResponse(
    var WishlistList: ArrayList<WishListGoodsItem>,
    var WishlistPositionList: ArrayList<WishListItemList>,
    var WishlistGoodsList: ArrayList<WishListGoods>
) : BaseResponse()

data class WishListGoods(
    var Query: String,
    var GoodsItemList: ArrayList<GoodsItem>
)

data class WishListGoodsItem(
    var Id: String,
    var Name: String
)

data class WishListItemList(
    var Id: String,
    var Keyword: String,
    var Checked: Int,
    var WishlistId: String
)