package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Функционально: поиск объектов «товар в корзине».
 */
data class CartItemSearchRequest(
    /**
     * Отступ выборки
     */
    @SerializedName("Count") var Count: String,

    /**
     * Ограничение выборки
     */
    @SerializedName("Offset") var Offset : String
)

