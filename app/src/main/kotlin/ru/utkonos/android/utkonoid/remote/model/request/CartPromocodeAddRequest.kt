package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartPromocodeAddRequest(
   @SerializedName("Code") var Code:String
)