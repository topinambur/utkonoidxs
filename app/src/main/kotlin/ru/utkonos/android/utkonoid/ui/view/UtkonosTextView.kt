package ru.utkonos.android.utkonoid.ui.view

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import ru.utkonos.android.utkonoid.R

class UtkonosTextView : AppCompatTextView {


    constructor(context: Context) : super(context) {
        if (!isInEditMode) {
            val face = Typeface.createFromAsset(context.assets, context.getString(R.string.bold))
            this.typeface = face
        }
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        if (!isInEditMode) {
            val face = Typeface.createFromAsset(context.assets, context.getString(R.string.bold))
            this.typeface = face
        }
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        if (!isInEditMode) {
            val face = Typeface.createFromAsset(context.assets, context.getString(R.string.bold))
            this.typeface = face
        }
    }

    //protected void onDraw(Canvas canvas) {
    //    super.onDraw(canvas);
    //}

    override fun performClick(): Boolean {
        return super.performClick()
    }

}
