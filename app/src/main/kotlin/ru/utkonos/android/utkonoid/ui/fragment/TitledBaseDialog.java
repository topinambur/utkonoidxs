package ru.utkonos.android.utkonoid.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import ru.utkonos.android.utkonoid.R;

public class TitledBaseDialog extends Fragment {

    private static final int DIALOG_WIDTH = 1024;
    private static final int DIALOG_HEIGHT = 312;

    private Fragment mParentFragment;
    private FragmentActivity mParentActivity;

    public static TitledBaseDialog newInstance() {
        TitledBaseDialog f = new TitledBaseDialog();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout mainContainer = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutparams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        mainContainer.setLayoutParams(layoutparams);
        mainContainer.setBackgroundColor(0x73000000);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        FrameLayout.LayoutParams contentLayoutParams = new FrameLayout.LayoutParams( (int) (DIALOG_WIDTH * displayMetrics.density), (int) (DIALOG_HEIGHT * displayMetrics.density), Gravity.CENTER );
        View v = inflater.inflate(R.layout.fragment_base_dialog, container, false);
        v.setLayoutParams( contentLayoutParams );
        v.setBackgroundResource( R.drawable.background_dialog );

        mainContainer.addView(v);

        return mainContainer;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            view.findViewById(R.id.close).setOnClickListener(v -> dissmiss());
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public void show(Fragment fragment, String tag) throws NullPointerException{
        mParentFragment = fragment;
        FragmentTransaction ft = mParentFragment.getChildFragmentManager().beginTransaction();
        if( mParentFragment.getView().getId() == -0x1 ){
            mParentFragment.getView().setId( ViewCompat.generateViewId() );
        }
        ft.add(mParentFragment.getView().getId(),this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void show(FragmentActivity activity, String tag) throws NullPointerException{
        mParentActivity = activity;
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(android.R.id.content,this, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void dissmiss() throws NullPointerException{
        if( mParentFragment != null ) {
            mParentFragment.getChildFragmentManager().beginTransaction().remove(this).commit();
        } else if( mParentActivity != null ){
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }

}
