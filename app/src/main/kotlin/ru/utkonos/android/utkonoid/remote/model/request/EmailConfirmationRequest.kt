package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class EmailConfirmationRequest(
   @SerializedName("Code") var Code:String
)