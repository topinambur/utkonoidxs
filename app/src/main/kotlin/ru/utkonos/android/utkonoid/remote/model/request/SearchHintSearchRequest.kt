package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SearchHintSearchRequest(
    @SerializedName("Text") var Text: String
)

