package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class CartLookupResponse(
        @SerializedName("CartList") var CartList: List<Cart>,
        @SerializedName("TotalCost") var TotalCost: String,

        /**
         * Рекомендации
         */
        //@SerializedName("GoodsRecommendationList") var GoodsRecommendationList: ArrayList<GoodsItem>,

        /**
         * Возможно вы забыли купить
         */
        @SerializedName("GoodsForgottenList") var GoodsForgottenList: List<GoodsItem>,

        @SerializedName("GoodsItemList") var GoodsItemList: List<GoodsItem>,

        @SerializedName("StarsGoodsInBasket") var StarsGoodsInBasket: List<GoodsItem>,

        /**
         * Доступные для редактирования заказы. Следует проверить поле EditableUntil
         */
        @SerializedName("OrderList") var OrderList:List<Order>
) : BaseResponse()