package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderModifyRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("Canceled") var Canceled:String,
   @SerializedName("PaymentTypeId") var PaymentTypeId:String
)