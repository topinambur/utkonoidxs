package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class DashbuttonSmsSaveRequest(
   @SerializedName("ButtonGuid") var ButtonGuid:String,
   @SerializedName("SmsText") var SmsText:String
)