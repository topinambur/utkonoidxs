package ru.utkonos.android.utkonoid.ui

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import ru.utkonos.android.utkonoid.ui.injection.DaggerApplicationComponent
import javax.inject.Inject

class UtkonosApplication : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject lateinit var mActivityInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
                .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return mActivityInjector
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mFragmentInjector
    }

}