package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.remote.model.Vacancy

data class CareerVacancySearchByIdResponse(
   @SerializedName("VacancyList") var  VacancyList : List<Vacancy>
) : BaseResponse()