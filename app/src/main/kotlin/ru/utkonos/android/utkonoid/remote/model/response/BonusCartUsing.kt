package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Информация по использованию бонусных баллов
 *
 * Флаг Delivery указывает, что пользователь выбрал оплату доставки бонусами, при этом, если интервал будет сброшен, DeliveryCost и DeliveryUsing могут быть равны "0".
 * В настоящее время 1 бонусный балл = 1 рубль, поэтому DeliveryCost=DeliveryUsing и GoodsCost=GoodsUsing, если курс изменится, эти значения будут различаться.
 */
data class BonusCartUsing(
    /**
     * Выбрана оплата доставки бонусными баллами
     */
    @SerializedName("Delivery") var Delivery: Int,

    /**
     * Стоимость доставки
     */
    @SerializedName("DeliveryCost") var DeliveryCost: Double,

    /**
     * Количество потраченных на доставку бонусных баллов
     */
    @SerializedName("DeliveryUsing") var DeliveryUsing: Double,

    /**
     * Стоимость материалов, которая будет погашена бонусами
     */
    @SerializedName("GoodsCost") var GoodsCost: Int,

    /**
     * Количество потраченных на оплату материалов бонусных баллов
     */
    @SerializedName("GoodsUsing") var GoodsUsing: Int
)