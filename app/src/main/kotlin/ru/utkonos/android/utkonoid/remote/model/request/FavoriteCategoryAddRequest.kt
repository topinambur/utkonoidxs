package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteCategoryAddRequest(
   @SerializedName("Title") var Title:String,
   @SerializedName("ParentId") var ParentId:String
)