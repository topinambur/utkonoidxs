package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class AreaDeliveryPointResponse(
    @SerializedName("AreaPointList") val areaPointList: List<AreaDeliveryPoint>
)