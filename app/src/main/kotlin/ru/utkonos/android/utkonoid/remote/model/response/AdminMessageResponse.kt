package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class AdminMessageResponse(
    @SerializedName("AdminMessageList") var AdminMessageList : List<AdminMessage>
) : BaseResponse()
