package ru.utkonos.android.utkonoid.ui.injection.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import ru.utkonos.android.utkonoid.ui.fragment.main.MainPageFragment

@Subcomponent
interface MainPageSubcomponent : AndroidInjector<MainPageFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainPageFragment>()

}