package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class Area(
    @SerializedName("Id") val Id: String,
    @SerializedName("Description") val Description: String,
    @SerializedName("MinLongitude") val MinLongitude: Double,
    @SerializedName("MinLatitude") val MinLatitude: Double,
    @SerializedName("MaxLongitude") val MaxLongitude: Double,
    @SerializedName("MaxLatitude") val MaxLatitude: String
)