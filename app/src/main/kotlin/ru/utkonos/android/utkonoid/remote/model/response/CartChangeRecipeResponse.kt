package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.remote.model.Vacancy

data class CartChangeRecipeResponse(
   @SerializedName("Cart") var  Cart : Cart,
   @SerializedName("Basket") var Basket : List<CartItem>
) : BaseResponse()