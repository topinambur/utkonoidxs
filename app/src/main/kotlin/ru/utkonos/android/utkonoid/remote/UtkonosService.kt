package ru.utkonos.android.utkonoid.remote

import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*
import ru.utkonos.android.utkonoid.remote.converterfactory.WrappedRequest
import ru.utkonos.android.utkonoid.remote.model.request.*
import ru.utkonos.android.utkonoid.remote.model.response.*


/**
 * Определяет абстрактные методы, используемые для взаимодействия с Utkonos API
 */
interface UtkonosService {

    @Multipart @POST("/api/rest/?")
    fun AccessRequestAdd( @Part("request") request : WrappedRequest<AccessRequestAddRequest>) : Single<ArrayList<Any>>

    @Multipart @POST("/api/rest/?")
    fun AdminMessageDelete( @Part("request") request : WrappedRequest<AdminMessageDeleteRequest>) : Single<ArrayList<Any>>

    @Multipart @POST("/api/rest/?")
    fun AdminMessageReply( @Part("request") request : WrappedRequest<AdminMessageReplyRequest>) : Single<ArrayList<Any>>

    @Multipart @POST("/api/rest/?")
    fun AdminMessageSearch( @Part("request") request : WrappedRequest<Unit> ) : Single<AdminMessageResponse>

    @Multipart @POST("/api/rest/?")
    fun AreaDeliveryPointGet( @Part("request") request : WrappedRequest<Unit> ) : Single<AreaDeliveryPointResponse>

    @Multipart @POST("/api/rest/?")
    fun AreaPointSearch( @Part("request") request : WrappedRequest<AreaPointSearchRequest> ) : Single<AreaDeliveryPointResponse>

    @Multipart @POST("/api/rest/?")
    fun AreaSearch( @Part("request") request : WrappedRequest<AreaSearchRequest> ) : Single<AreaSearchResponse>

    @Multipart @POST("/api/rest/?")
    fun BannerMainSearch( @Part("request") request : WrappedRequest<Unit> ) : Single<BannerMainSearchResponse>

    @Multipart @POST("/api/rest/?")
    fun BonusTransactionSearch( @Part("request") request : WrappedRequest<Unit> ) : Single<BonusTransactionSearchResponse>

    @Multipart @POST("/api/rest/?")
    fun BrandSearch( @Part("request") request : WrappedRequest<BrandSearchRequest> ) : Single<BrandSearchResponse>

    @Multipart @POST("/api/rest/?")
    fun CareerDepartmentSearch(@Part("request") request : WrappedRequest<CareerDepartmentSearchRequest> ) : Single<CareerDepartmentSearchResponse>

    @Multipart @POST("/api/rest/?")
    fun CareerVacancySearchById( @Part("request") request : WrappedRequest<CareerVacancySearchByIdRequest> ) :Single<CareerVacancySearchByIdResponse>

    @Multipart @POST("/api/rest/?")
    fun CartBonusAdd(@Part("request") request : WrappedRequest<CartBonusAddRequest> ) :Single<CartResponse>

    @Multipart @POST("/api/rest/?")
    fun CartChangeRecipe(@Part("request") request : WrappedRequest<CartChangeRecipeRequest>) : Single<CartChangeRecipeResponse>

    @Multipart @POST("/api/rest/?")
    fun CartFromOrderAdd(@Part("request") request : WrappedRequest<CartFromOrderAddRequest>) : Single<CartResponse>

    @Multipart @POST("/api/rest/?")
    fun CartFromTemplateAdd(@Part("request") request : WrappedRequest<CartFromTemplateAddRequest>) : Single<CartResponse>

    @Multipart @POST("/api/rest/?")
    fun CartItemAdd(@Part("request") request : WrappedRequest<CartItemAddRequest>) : Single<CartResponse>




    @Multipart @POST("/api/rest/?")
    fun CartLookup( @Part("request") request : WrappedRequest<CartLookupRequest>) : Single<CartLookupResponse>

    @Multipart @POST("/api/rest/?")
    fun MainpageGet( @Part("request") request : WrappedRequest<MainpageGetRequest>) : Single<MainpageGetResponse>

    @Multipart @POST("/api/rest/?")
    fun upload(@Part("request") request : RequestBody, @Part("file\"; filename=\"pp.jpg\" ") fileUpload : RequestBody) : Single<UserLookupResponse>

}

//fun AdminMessageSearch( @Query("request") request : String ) : Single<AdminMessageResponse>