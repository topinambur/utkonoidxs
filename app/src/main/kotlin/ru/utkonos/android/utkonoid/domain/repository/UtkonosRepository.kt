package ru.utkonos.android.utkonoid.domain.repository

import io.reactivex.Single
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse

/**
 * Interface defining methods for how the data layer can pass data to and from the Domain layer.
 * This is to be implemented by the data layer, setting the requirements for the
 * operations that need to be implemented
 */
interface UtkonosRepository {

    fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse>

}