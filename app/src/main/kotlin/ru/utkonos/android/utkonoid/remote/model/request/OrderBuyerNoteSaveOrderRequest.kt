package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderBuyerNoteSaveOrderRequest(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("Text") var Text:String
)