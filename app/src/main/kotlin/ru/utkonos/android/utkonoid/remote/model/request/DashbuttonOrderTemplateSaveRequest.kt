package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class DashbuttonOrderTemplateSaveRequest(
   @SerializedName("ButtonGuid") var ButtonGuid:String,
   @SerializedName("Day") var Day:String,
   @SerializedName("Interval") var Interval:String,
   @SerializedName("BasketTemplateId") var BasketTemplateId:String,
   @SerializedName("PavOrderId") var PavOrderId:String,
   @SerializedName("ClearCartBefore") var ClearCartBefore:String,
   @SerializedName("UserAddressId") var UserAddressId:String
)