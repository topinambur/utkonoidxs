package ru.utkonos.android.utkonoid.remote.model.response

data class Image(
    var SmallUrl: String,
    var BigUrl: String,
    var HighUrl: String
)