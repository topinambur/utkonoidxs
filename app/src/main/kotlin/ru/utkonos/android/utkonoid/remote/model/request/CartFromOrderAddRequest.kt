package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartFromOrderAddRequest(
    @SerializedName("OrderId") var OrderId:String,
    @SerializedName("Source")  var Source:String,
    @SerializedName("Return")  var Return:CartFromOrderAddReturn
)

data class CartFromOrderAddReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)