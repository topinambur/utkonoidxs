package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsItemBestSearchRequest(
   @SerializedName("GoodsItemBestSearch") var GoodsItemBestSearch:String
)