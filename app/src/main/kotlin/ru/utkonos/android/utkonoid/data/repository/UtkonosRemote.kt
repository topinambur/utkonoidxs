package ru.utkonos.android.utkonoid.data.repository

import io.reactivex.Single
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse

/**
 * Интерфейс определяет методы для получения данных из Utkonos.
 */
interface UtkonosRemote {
    fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse>
}