package ru.utkonos.android.utkonoid.remote.model.response

import java.util.ArrayList

data class WishListResponse(
        var WishlistList: List<WishListItem>,
        var WishlistItemList: List<WishListItemList>
) : BaseResponse()

data class WishListItem(
    var Id: String,
    var Name: String,
    var WishListItemList: List<WishListItemList>
)