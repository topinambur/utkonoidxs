package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ReferenceLookupRequest(
    @SerializedName("Name") var Name:String,
    @SerializedName("Return") var Return:ReturnReference
)

data class ReturnReference(
    @SerializedName("TextFilter") var TextFilter: String
)