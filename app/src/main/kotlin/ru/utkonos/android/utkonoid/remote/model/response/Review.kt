package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Ilya Solovyov on 24.05.2018.
 * is3k@ya.ru
 */
data class Review(
        @SerializedName("BuyerName") val buyerName: String,
        @SerializedName("Id") val id: String,
        @SerializedName("Text") val text: String,
        @SerializedName("Created") val created: String,
        @SerializedName("Avatar") val avatar: String
)