package ru.utkonos.android.utkonoid.remote.model.response

data class Comment(
    var Author: String,
    var Created: String,
    var Id: Int,
    var ParentId: Int,
    var Text: String,
    var RateUp: String,
    var RateDown: String,
    var Stars: Int,
    var UserPic: String
)