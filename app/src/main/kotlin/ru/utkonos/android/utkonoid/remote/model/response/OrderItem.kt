package ru.utkonos.android.utkonoid.remote.model.response

data class OrderItem(
    var Id: String,
    var ImageSmallUrl: String,
    var ImageUrl: String,
    var Name: String,
    var Price: Double,
    var Quantity: Int,
    var QuantityOrigin: Int,
    var PriceSold: Double,
    var PriceWeight: Double,
    var PriceWeightSold: Double,
    var userFeedbackProblemId: Int,
    var userFeedbackComment: String
)