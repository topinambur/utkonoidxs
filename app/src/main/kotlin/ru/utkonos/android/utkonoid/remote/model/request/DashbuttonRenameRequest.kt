package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class DashbuttonRenameRequest(
   @SerializedName("ButtonGuid") var ButtonGuid:String,
   @SerializedName("Name") var Name:String
)