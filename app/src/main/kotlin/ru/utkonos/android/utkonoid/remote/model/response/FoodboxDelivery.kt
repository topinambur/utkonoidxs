package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class FoodboxDelivery(
        @SerializedName("TerminalId") var TerminalId: String,
        @SerializedName("Title") var Title: String
)