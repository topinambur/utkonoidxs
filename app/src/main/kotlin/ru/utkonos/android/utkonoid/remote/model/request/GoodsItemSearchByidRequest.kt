package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsItemSearchByidRequest(
   @SerializedName("Id") var Id:String
)