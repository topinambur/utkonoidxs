package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteCategoryDeleteRequest(
    @SerializedName("Id") var Id:String,
    @SerializedName("Return") var Return: FavoriteCategoryDeleteReturn
)

data class FavoriteCategoryDeleteReturn(
    @SerializedName("FavoriteItem") var FavoriteItem:Boolean
)