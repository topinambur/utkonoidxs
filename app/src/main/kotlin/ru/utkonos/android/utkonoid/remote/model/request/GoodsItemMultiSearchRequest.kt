package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsItemMultiSearchRequest(
   @SerializedName("Query") var Query:String
)