package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Функционально: редактирование объекта «корзина».
 */
data class CartModifyRequest(
   /**
    * Интервал доставки
    */
   @SerializedName("DeliveryIntervalId") var DeliveryIntervalId:String,
   @SerializedName("DontCallDoor") var Client:Boolean = false,
   @SerializedName("DeliveryPremature") var DeliveryPremature:Boolean = false,
   @SerializedName("PassRequired") var PassRequired:Boolean = false
)