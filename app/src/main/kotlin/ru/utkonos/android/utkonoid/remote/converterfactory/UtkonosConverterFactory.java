package ru.utkonos.android.utkonoid.remote.converterfactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UtkonosConverterFactory extends Converter.Factory {

    private GsonConverterFactory factory;

    public UtkonosConverterFactory(GsonConverterFactory factory) {
        this.factory = factory;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(final Type type, Annotation[] annotations, Retrofit retrofit) {
        Type wrappedType = new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                // -> WrappedResponse<type>
                return new Type[]{type};
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public Type getRawType() {
                return WrappedResponse.class;
            }
        };

        Converter<ResponseBody, ?> gsonConverter = factory.responseBodyConverter(wrappedType, annotations, retrofit);

        return new UtkonosResponseBodyConverter(gsonConverter);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(final Type type, Annotation[] parameterAnnotations, Annotation[] annotations, Retrofit retrofit) {

        return factory.requestBodyConverter(type, parameterAnnotations, annotations, retrofit);

        /*Type wrappedType = new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                // -> WrappedResponse<type>
                return new Type[]{type};
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public Type getRawType() {
                return WrappedRequest.class;
            }
        };

        Converter<?, RequestBody> gsonConverter = factory.requestBodyConverter(wrappedType, parameterAnnotations, annotations, retrofit);

        return new UtkonosRequestBodyConverter(gsonConverter, head);*/
    }

}