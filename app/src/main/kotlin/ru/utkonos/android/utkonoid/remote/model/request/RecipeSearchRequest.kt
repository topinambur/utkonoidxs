package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class RecipeSearchRequest(
   @SerializedName("Count") var Count:String,
   @SerializedName("Id") var Id:String,
   @SerializedName("CategoryId") var CategoryId:String,
   @SerializedName("Offset") var Offset:String,
   @SerializedName("Return") var Return:RecipeSearchReturn
)

data class RecipeSearchReturn(
    @SerializedName("Cooking") var Cooking: Boolean,
    @SerializedName("Ingredient") var Ingredient: Boolean,
    @SerializedName("Goods") var Goods: Boolean,
    @SerializedName("BestRecipes") var BestRecipes: Boolean,
    @SerializedName("Comments") var Comments: Boolean
)