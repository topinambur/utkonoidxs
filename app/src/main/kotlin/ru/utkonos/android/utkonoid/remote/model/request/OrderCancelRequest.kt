package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderCancelRequest(

    /**
     * Идентификатор заказа
     */
   @SerializedName("Id") var Id:String,

   /**
    * Выполнить проверку дозаказов
    * Если передан флаг "Evaluate" отличный от "0", будет произведена проверка дозаказов.
    * Если проверка покажет, что отмена указанного заказа повлечет отмену дозаказов,
    * заказ отменен не будет, а результирующее сообщение будет содержать информацию по дозаказам.
    * Вместо этого поля можно воспользоваться методом orderCancelEvaluate.
    *
    */
   @SerializedName("Evaluate") var Evaluate:String
)