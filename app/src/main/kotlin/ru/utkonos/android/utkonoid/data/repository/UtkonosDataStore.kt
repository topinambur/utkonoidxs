package ru.utkonos.android.utkonoid.data.repository

import io.reactivex.Single
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse

/**
 * Interface defining methods for the data operations related to Bufferroos.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented
 */
interface UtkonosDataStore {

    fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse>

}