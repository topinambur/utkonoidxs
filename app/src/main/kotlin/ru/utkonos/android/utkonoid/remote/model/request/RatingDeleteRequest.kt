package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class RatingDeleteRequest(
    @SerializedName("GoodId") var GoodId: String
)

