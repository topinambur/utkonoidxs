package ru.utkonos.android.utkonoid.remote.model.response

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

/**
 * Заказ. Возвращается в методах orderAdd, orderLookup и orderSearch.
 */
data class Order(

        /**
         * ПК заказа
         */
        @SerializedName("Id") var Id: String,

        /**
         * Стоимость товаров БЕЗ скидки и БЕЗ доставки
         */
        @SerializedName("BaseGoodsCost") var BaseGoodsCost: Double,

        /**
         * Возможность отмены заказа
         */
        @SerializedName("Cancelable") var Cancelable: Boolean,

        /**
         * О заказе оставлен отзыв
         */
        @SerializedName("FeedBack") var FeedBack: Boolean,

        @SerializedName("PaymentAllowApplePay") var paymentAllowApplePay: Boolean,

        @SerializedName("PaymentAllowAndroidPay") var paymentAllowAndroidPay: Boolean,

        @SerializedName("PaymentAllowSamsungPay") var paymentAllowSamsungPay: Boolean,

        /**
         * Время создания заказа
         */
        @SerializedName("Created") var Created: String,

        /**
         * Адрес доставки (кратко)
         */
        @SerializedName("DeliveryAddress") var DeliveryAddress: String,

        @SerializedName("DeliveryDate") var DeliveryDate: String,

        @SerializedName("DeliveryTimeFrom") var DeliveryTimeFrom: String,

        @SerializedName("DeliveryTimeTo") var DeliveryTimeTo: String,

        /**
         * Время доставки
         */
        @SerializedName("DeliveryTimeTitle") var DeliveryTimeTitle:String,

        /**
         * Время, до которого возможно редактирование заказа.
         */
        @SerializedName("EditableUntil") var EditableUntil: String,

        /**
         * Тип доставки
         * 2 - Доставка по адресу
         * 5 - Доставка в продуктомат
         * 9 - Доставка в пикпоинт
         * 11 - Стронгпоинт
         */
        @SerializedName("DeliveryTypeId") var DeliveryTypeId: Int,

        /**
         * Стоимость доставки
         */
        @SerializedName("DeliveryPrice") var DeliveryPrice: Double,

        /**
         * Скидка на товары
         */
        @SerializedName("DiscountValue") var DiscountValue: Double,

        /**
         * Павильон
         */
        @SerializedName("PavilionId") var PavilionId : String,

        /**
         *
         */
        @SerializedName("PavilionOrderId") var PavilionOrderId: String,


        /**
         * ex: A2+79256460592&2532
         */
        @SerializedName("FoodboxReceiveQr") var FoodboxReceiveQr: String,


        /**
         * ex: 2532
         */
        @SerializedName("FoodboxReceiveCode") var FoodboxReceiveCode: String,

        /**
         * Тип оплаты
         *
         * case uponReceipt = 1
         * case byCard = 2
         * case installmentPayment = 10
         * case applePay = 102
         * case inFoodbox = 103
         * case androidPay = 112
         * case samsungPay = 204
         * case googlePay = 210
         * case unknown = 0
         */
        @SerializedName("PaymentTypeId") var PaymentTypeId: Int,

        /**
         * Код статуса оплаты
         *
         * case waiting = 0
         * case prepaid = 1
         * case failure = -1
         * case paid = 2
         * case canceled = 3
         * case partialReturn = 44
         * case verification = 5
         * case debit = 7
         * case timeout = 8
         * case refund = 9
         * case unknown = -99
         */
        @SerializedName("PaymentStatusCode") var PaymentStatusCode: String,

        /**
         * Текст статуса оплаты
         */
        @SerializedName("PaymentStatusText") var PaymentStatusText: String,

        /**
         * Страница предоплаты
         */
        @SerializedName("PaymentUrl") var PaymentUrl: String,

        /**
         * Группировка по статусу: "accepted", "paid", "canceled"
         */
        @SerializedName("GroupStatus") var GroupStatus: String,

        /**
         * Выставленный пользователем рейтинг
         */
        @SerializedName("Rating") var Rating: Int,

        /**
         * Код статуса
         */
        @SerializedName("StatusCode") var StatusCode: Int,

        @SerializedName("StatusText") var StatusText: String,

        @SerializedName("Status") var status: OrderStatus,

        /**
         * Оплаченная сумма (включает бонусы)
         */
        @SerializedName("PaySum") var PaySum: Double,
        @SerializedName("GoodsCost") var GoodsCost: String,
        @SerializedName("TotalCost") var TotalCost: Double,
        @SerializedName("TotalWeight") var TotalWeight: Double,
        @SerializedName("BonusSum") var BonusSum: String,
        @SerializedName("BonusDeliveryCost") var BonusDeliveryCost: String,

        /**
         * Терминал получения товара для точек выдачи заказов
         */
        @SerializedName("FoodboxId") var FoodboxId: String,

        /**
         * Сумма товаров, оплаченная бонусами
         */
        @SerializedName("BonusGoodsCost") var BonusGoodsCost: String,

        /**
         * Сумма, оплаченная бонусами при оформлении заказа
         */
        @SerializedName("BaseBonusSum") var BaseBonusSum: String,

        /**
         * Начислено бонусов за оплату заказа
         */
        @SerializedName("BonusPlusAmount") var BonusPlusAmount: Int,


        /**
         * Примечание покупателя к заказу (если есть). См OrderBuyerNoteSaveOrder, OrderBuyerNoteSaveCart
         */
        @SerializedName("OrderBuyerNote") var OrderBuyerNote: String,

        /**
         * Возможность оплатить заказ
         * Возможность изменения предоплаты
         */
        @SerializedName("PrepayChangeOpportunity") var PrepayChangeOpportunity: PrepayChangeOpportunity,

        /**
         * Список заказанных товаров
         */
        @SerializedName("OrderItemList") var OrderItemList: ArrayList<OrderItem>,

        @SerializedName("GoodsItemList") var GoodsItemList: ArrayList<GoodsItem>,

        /**
         * Доступность маршрута следования. Если флаг не передан, значит маршрут недоступен.
         */
        @SerializedName("TripPathAvailable") var TripPathAvailable: Boolean,

        /**
         * Информация по маршруту
         */
        @SerializedName("TruckTripList") var TruckTripList: ArrayList<TruckTrip>,

        /**
         * Данные о движении заказа по маршруту
         */
        @SerializedName("TruckTripPathList") var TruckTripPathList: ArrayList<TruckTripPath>,

        /**
         * Контрольные точки (заказы) на маршруте
         */
        @SerializedName("TruckTripPointList") var TruckTripPointList: ArrayList<TruckTripPoint>

)