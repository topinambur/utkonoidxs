package ru.utkonos.android.utkonoid.data.mapper

import ru.utkonos.android.utkonoid.data.model.AdminMessageListEntity
import ru.utkonos.android.utkonoid.remote.model.response.AdminMessage
import javax.inject.Inject


open class AdminMessageListMapper @Inject constructor(): Mapper<AdminMessageListEntity, List<AdminMessage>> {

    /**
     * Map a [AdminMessageListEntity] instance to a [List<AdminMessage>] instance
     */
    override fun mapFromEntity(type: AdminMessageListEntity): List<AdminMessage> {
        return type.adminMessageList
    }

    /**
     * Map a [List<AdminMessage>] instance to a [AdminMessageListEntity] instance
     */
    override fun mapToEntity(type: List<AdminMessage>): AdminMessageListEntity {
        return AdminMessageListEntity(type)
    }


}