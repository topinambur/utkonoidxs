package ru.utkonos.android.utkonoid.remote.model.response

data class BonusAccount(
    var Id: String,
    var UserId: String,
    var Balance: Int,
    var BalanceAction: Int,
    var BalanceTrans: Int,
    var ExpirationTrans: String,
    var ExpirationAction: String,
    var Enabled: Int
)