package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ReferralSendInviteRequest(
    @SerializedName("FriendPhone") var FriendPhone:String,
    @SerializedName("FriendName") var FriendName:String
)