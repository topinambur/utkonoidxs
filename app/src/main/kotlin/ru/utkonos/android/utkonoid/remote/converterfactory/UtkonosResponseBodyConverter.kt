package ru.utkonos.android.utkonoid.remote.converterfactory

import java.io.IOException
import okhttp3.ResponseBody
import retrofit2.Converter
import ru.utkonos.android.utkonoid.data.model.UtkonosError
import ru.utkonos.android.utkonoid.remote.model.response.BaseResponse


class UtkonosResponseBodyConverter<T>(private val converter: Converter<ResponseBody, WrappedResponse<T>>) : Converter<ResponseBody, T> {

    @Throws(IOException::class)
    override fun convert(value: ResponseBody): T {

        val response = converter.convert(value)

        when {
            response.head.status.equals("success", ignoreCase = true) -> return response.body
            response.head.status.equals("pending", ignoreCase = true) -> return response.body
            response.head.status.equals("failure", ignoreCase = true) -> {
                if( response.body !is BaseResponse) {
                    throw IOException("Incompatible type of Body")
                }

                val errorList: ArrayList<UtkonosError> = (response.body as BaseResponse).ErrorList
                if (!errorList.isEmpty()) {
                    throw UtkonosAPIException((response.body as BaseResponse).ErrorList[0])
                }

                throw IOException( "Unknown Utkonos API exception" )
            }
            else -> throw IOException("Utkonos API exception")
        }

    }


}
