package ru.utkonos.android.utkonoid.remote.model.response

data class BonusTransaction(
    var Txn: String,
    var TypeId: Int,
    var TypeShort: String,
    var OrderId: String,
    var OrderSum: Double,
    var OrderPaySum: Double,
    var BonusedOrderSum: Double,
    var Finished: String,
    var Amount: Int,
    var AmountAction: Int,
    var AmountTrans: Int
)