package ru.utkonos.android.utkonoid.data.model

import ru.utkonos.android.utkonoid.remote.model.response.AdminMessage

/**
 * Representation for a [AdminMessageListEntity] fetched from an external layer data source
 */
data class AdminMessageListEntity(val adminMessageList : List<AdminMessage>)