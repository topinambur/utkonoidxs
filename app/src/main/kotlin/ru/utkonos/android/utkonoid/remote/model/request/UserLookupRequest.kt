package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserLookupRequest(
    @SerializedName("Source") var Source:String,
    @SerializedName("CartReturn") var Return: UserLookupReturn
)

data class UserLookupReturn(
    @SerializedName("AllowRemainsQuantity") var AllowRemainsQuantity:String,
    @SerializedName("EditableOrderList") var EditableOrderList:String,
    @SerializedName("Goods") var Goods:String,
    @SerializedName("GoodsForgottenList") var GoodsForgottenList:String,
    @SerializedName("GoodsRecommendationList") var GoodsRecommendationList:String,
    @SerializedName("OrderDeliveryOffer") var OrderDeliveryOffer:String,
    @SerializedName("StarsGoodsInBasket") var StarsGoodsInBasket:String
)