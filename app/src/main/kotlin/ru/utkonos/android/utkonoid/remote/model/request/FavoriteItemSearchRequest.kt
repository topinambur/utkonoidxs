package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteItemSearchRequest(
   @SerializedName("Count") var Count:String,
   @SerializedName("Offset") var Offset:String,
   @SerializedName("SearchText") var SearchText:String,
   @SerializedName("CategoryId") var CategoryId:String,
   @SerializedName("OrderPreset") var OrderPreset:String
)