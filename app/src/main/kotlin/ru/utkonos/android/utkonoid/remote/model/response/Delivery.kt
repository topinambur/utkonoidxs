package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class Delivery(
    @SerializedName("DontCallDoor") var DontCallDoor: Boolean,
    @SerializedName("DeliveryPremature") var DeliveryPremature: Boolean,
    @SerializedName("PassRequired") var PassRequired: Boolean
)