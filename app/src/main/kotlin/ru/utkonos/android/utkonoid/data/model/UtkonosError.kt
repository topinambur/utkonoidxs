package ru.utkonos.android.utkonoid.data.model

import com.google.gson.annotations.SerializedName

data class UtkonosError (
    @SerializedName("Class")       val Class : String,
    @SerializedName("Code")        val Code: String,
    @SerializedName("Message")     val Message: String,
    @SerializedName("Description") val Description: String,
    @SerializedName("Type")        val Type: String
)