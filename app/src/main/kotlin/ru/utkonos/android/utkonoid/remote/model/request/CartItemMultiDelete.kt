package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemMultiDeleteRequest(
        @SerializedName("GoodsItemId") var GoodsItemId:String,
        @SerializedName("Return")  var Return: CartItemMultiDeleteReturn
)

data class CartItemMultiDeleteReturn(
        @SerializedName("Cart") var Cart: Boolean,
        @SerializedName("CartItemList") var CartItemList:Boolean,
        @SerializedName("TotalCost") var TotalCost:Boolean
)