package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartBonusAddRequest(
    @SerializedName("GoodsUsing") var GoodsUsing:String,
    @SerializedName("Return") var Return:CartBonusAddReturn
)

data class CartBonusAddReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)