package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class AreaPointSearchRequest(
   @SerializedName("AreaId") var AreaId:String = "696"
)