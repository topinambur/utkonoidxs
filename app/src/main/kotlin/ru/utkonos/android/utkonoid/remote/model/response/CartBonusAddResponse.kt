package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.remote.model.Vacancy

data class CartBonusAddResponse(
   @SerializedName("CartList") var  CartList : List<Cart>,
   @SerializedName("CartItemList") var CartItemList : List<CartItem>,
   @SerializedName("TotalCost") var TotalCost:String
) : BaseResponse()