package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class AccessRequestAddRequest(
   @SerializedName("Field") var Field:String
)