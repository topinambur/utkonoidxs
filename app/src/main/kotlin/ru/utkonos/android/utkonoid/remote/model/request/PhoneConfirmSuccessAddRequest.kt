package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PhoneConfirmSuccessAddRequest(
    @SerializedName("Phone") var Phone:String,
    @SerializedName("Code") var Code:String
)
