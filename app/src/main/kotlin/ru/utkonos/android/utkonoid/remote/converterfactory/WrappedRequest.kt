package ru.utkonos.android.utkonoid.remote.converterfactory

import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.remote.model.request.UtkonosHeadRequest

/**
 * Завернутый запрос серверу, содержащий заголовок Head и Body указанного в интерфейсе retrofit типа
 */
open class WrappedRequest<T> (
    @SerializedName("Head") val head: UtkonosHeadRequest,
    @SerializedName("Body") val body: T
)

