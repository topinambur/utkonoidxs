package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class BonusTransactionSearchRequest(
   @SerializedName("Offset") var Offset:String,
   @SerializedName("Count") var Count:String
)