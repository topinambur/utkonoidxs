package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserLocationAddRequest(
    @SerializedName("Latitude") var Latitude:String,
    @SerializedName("Longitude") var Longitude: String,
    @SerializedName("Accuracy") var Accuracy: String
)
