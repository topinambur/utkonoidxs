package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PreorderCreateRequest(
    @SerializedName("ItemId") var ItemId:String,
    @SerializedName("Count") var Count:String
)
