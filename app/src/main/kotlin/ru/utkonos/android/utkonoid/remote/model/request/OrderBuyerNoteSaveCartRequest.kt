package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderBuyerNoteSaveCartRequest(
   @SerializedName("Text") var Text:String
)