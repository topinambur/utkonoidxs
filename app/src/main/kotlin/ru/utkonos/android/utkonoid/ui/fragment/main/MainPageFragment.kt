package ru.utkonos.android.utkonoid.ui.fragment.main

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import ru.utkonos.android.utkonoid.R
import ru.utkonos.android.utkonoid.presentation.main.MainFragmentContract
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import ru.utkonos.android.utkonoid.ui.fragment.TitledBaseDialog
import javax.inject.Inject

class MainPageFragment: Fragment(), MainFragmentContract.View{

    @Inject lateinit var onboardingPresenter: MainFragmentContract.Presenter

    companion object {
        fun newInstance(): MainPageFragment {
            return MainPageFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mainpage, container, false)
    }

    override fun setPresenter(presenter: MainFragmentContract.Presenter) {
        onboardingPresenter = presenter
    }

    override fun onStart() {
        super.onStart()
        onboardingPresenter.start()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun refreshMainPage(data: MainpageGetResponse) {
        val tbd = TitledBaseDialog.newInstance()
        tbd.show( this, "TitledBaseDialog" )
    }

    override fun showErrorState() {
    }

    override fun hideErrorState() {
    }

    override fun showEmptyState() {
    }

    override fun hideEmptyState() {
    }

}