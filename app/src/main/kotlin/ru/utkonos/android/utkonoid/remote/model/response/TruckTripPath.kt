package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class TruckTripPath(

     @SerializedName("Id") var Id: Int,

    /**
     * Идентификатор рейса TruckTrip
     */
     @SerializedName("TruckTripId") var TruckTripId: Int,

    /**
     * Номер точки по порядку объезда, если произошло связанное событие (прибытие или убытие)
     */
     @SerializedName("PointNumber") var PointNumber: Int,

    /**
     * Долгота
     */
     @SerializedName("Longitude") var Longitude: Float,

    /**
     * Широта
     */
     @SerializedName("Latitude") var Latitude: Float,

    /**
     * Время регистрации положения
     */
     @SerializedName("Time") var Time: String
)