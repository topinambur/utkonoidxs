package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderAppendRequest(
   @SerializedName("OrderId") var OrderId:String
)