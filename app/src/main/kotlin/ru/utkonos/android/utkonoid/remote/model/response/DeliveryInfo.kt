package ru.utkonos.android.utkonoid.remote.model.response

data class DeliveryInfo(
    var MinOrder: Int,
    var Cost: Int,
    var Weight: Int,
    var Dimensions: Float,
    var MinCostClusterDelivery: Int,
    var FreeDeliveryCost: Int,
    var MinCostBasket: Int,
    var MinCostDeliveryForFoodbox: Int
)