package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteItemAddRequest(
   @SerializedName("FavoriteCategoryId") var FavoriteCategoryId:String,
   @SerializedName("GoodsItemId") var GoodsItemId:String
)