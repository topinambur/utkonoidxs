package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class LastComment(
    @SerializedName("Author") val author: String,
    @SerializedName("OwnerId") val ownerId: String,
    @SerializedName("Created") val created: String,
    @SerializedName("Id") val id: String,
    @SerializedName("ParentId") val parentId: String,
    @SerializedName("RateUp") val rateUp: String,
    @SerializedName("RateDown") val rateDown: String,
    @SerializedName("Text") val text: String,
    @SerializedName("UserPic") val userPic: String,
    @SerializedName("SelfCommentRate") val selfCommentRate: Int,
    @SerializedName("Stars") val stars: String,
    @SerializedName("GoodsItem") val goodsItem: GoodsItem
)