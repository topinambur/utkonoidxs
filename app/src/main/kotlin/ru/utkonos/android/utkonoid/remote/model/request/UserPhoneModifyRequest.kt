package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserPhoneModifyRequest(
   @SerializedName("Phone") var Phone:String,
   @SerializedName("ConfirmCode") var ConfirmCode:String
)