package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class AreaDeliveryPoint(
    @SerializedName("Id") val id: String,
    @SerializedName("AreaId") val areaId: String,
    @SerializedName("Longitude") val longitude: String,
    @SerializedName("Latitude") val latitude: String
)