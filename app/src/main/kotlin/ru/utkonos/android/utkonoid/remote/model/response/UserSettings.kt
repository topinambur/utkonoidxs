package ru.utkonos.android.utkonoid.remote.model.response

data class UserSettings(
    var Id: String,
    var SubscriptionSpecial: String,
    var SubscriptionSpecialSms: String,
    var ViberSpecialOffers: String,
    var SubscriptionOrderAdd: String,
    var SubscriptionOrderStatus: String,
    var SubscriptionOrderNotifySms: String,
    var SubscriptionBonusNotifySms: String,
    var ShowAbsentGoods: String
)