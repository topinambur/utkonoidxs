package ru.utkonos.android.utkonoid.remote.model

import com.google.gson.annotations.SerializedName

data class Department(
        @SerializedName("Id") var Id:String,
        @SerializedName("ParentId") var ParentId: String,
        @SerializedName("Enabled") var Enabled: String,
        @SerializedName("Title") var Title: String,
        @SerializedName("Body") var Body : String,
        @SerializedName("BodyMobile") var BodyMobile:String,
        @SerializedName("ShortDescription") var ShortDescription:String,
        @SerializedName("Shortcut") var Shortcut:String
)