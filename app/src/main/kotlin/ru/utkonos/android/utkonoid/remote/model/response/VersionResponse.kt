package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class VersionResponse(
    @SerializedName("Version") var Version: Version
) : BaseResponse()

data class Version(
    var Id: String,
    var Updated: String,
    var Version: String,
    var Description: String
)