package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemAllBystoreAddRequest(
    @SerializedName("DeliveryStoreId") var DeliveryStoreId:String,
    @SerializedName("Count") var Count:String
)