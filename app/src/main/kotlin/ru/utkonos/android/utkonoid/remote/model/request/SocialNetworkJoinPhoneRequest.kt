package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SocialNetworkJoinPhoneRequest(
    @SerializedName("Phone") var Phone:String
)