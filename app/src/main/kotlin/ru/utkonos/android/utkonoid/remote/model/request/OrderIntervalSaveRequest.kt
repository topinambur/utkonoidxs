package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderIntervalSaveRequest(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("IntervalId") var IntervalId:String
)