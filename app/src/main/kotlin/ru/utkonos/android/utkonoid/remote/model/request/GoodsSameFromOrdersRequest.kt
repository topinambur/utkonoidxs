package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsSameFromOrdersRequest(
   @SerializedName("CountLastOrders") var CountLastOrders:String
)