package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CareerVacancySearchByIdRequest(
        @SerializedName("VacancyId") var VacancyId:String,
        @SerializedName("Return") var Return:CareerVacancySearchByIdReturn
)

data class CareerVacancySearchByIdReturn(
        @SerializedName("Similar") var Similar: Boolean = true
)