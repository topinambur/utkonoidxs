package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class UtkonosHeadRequest(
    @SerializedName("SessionToken") val sessionToken : String = "",
    @SerializedName("Created") var created : String = "",
    @SerializedName("DeviceId") val deviceId : String = "1537192563",
    @SerializedName("MarketingPartnerKey") val marketingPartnerKey: String = "123123123",
    @SerializedName("Method") val method    : String = "",
    @SerializedName("RequestId") var requestId : String = "",
    @SerializedName("Username") val username  : String = "",
    @SerializedName("Password") val password  : String = "",
    @SerializedName("Version")  val version   : String = "1.0",
    @SerializedName("Client") val client : String = "",
    @SerializedName("AdvertisingId") val advertisingId : String = "",
    @SerializedName("AppsFlyerId") val appsFlyerId : String = ""
) {
    init{
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
        created = sdf.format(Date())
        requestId = UUID.randomUUID().toString()
    }
}