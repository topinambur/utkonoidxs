package ru.utkonos.android.utkonoid.remote.model.response

data class UserAddressListResponse(
    var UserAddressList: List<UserAddress>
) : BaseResponse()