package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsItemSearchRequest(
   @SerializedName("Barcode") var Barcode:String,
   @SerializedName("BarcodeHistory") var BarcodeHistory:String,
   @SerializedName("BrandId") var BrandId:String,
   @SerializedName("Count") var Count:String,
   @SerializedName("Filters") var Filters:String,
   @SerializedName("GoodsCategoryId") var GoodsCategoryId:String,
   @SerializedName("RootCategoryId") var RootCategoryId:String,
   @SerializedName("Id") var Id:String,
   @SerializedName("IgnoreStock") var IgnoreStock:Boolean,
   @SerializedName("Manufacturer") var Manufacturer:String,
   @SerializedName("ModelGroupingInside") var ModelGroupingInside:String,
   @SerializedName("Novelty") var Novelty:String,
   @SerializedName("Offset") var Offset:String,
   @SerializedName("OrderPreset") var OrderPreset:String,
   @SerializedName("PastPurchases") var PastPurchases:String,
   @SerializedName("Special") var Special:String,
   @SerializedName("CatalogueSpecial") var CatalogueSpecial:String,
   @SerializedName("Text") var Text:String,
   @SerializedName("Trend") var Trend:String,
   @SerializedName("IncludePreorder") var IncludePreorder:Boolean,
   @SerializedName("Return") var Return: GoodsItemSearchReturn
)

data class GoodsItemSearchReturn(
    @SerializedName("Barcode") var Barcode:Boolean,
    @SerializedName("Cart") var Cart:Boolean,
    @SerializedName("Description") var Description:Boolean,
    @SerializedName("GoodsCategoryList") var GoodsCategoryList:Boolean,
    @SerializedName("GoodsDescriptionList") var GoodsDescriptionList:Boolean,
    @SerializedName("Url") var Url:Boolean,
    @SerializedName("AllCrosslinks") var AllCrosslinks:Boolean,
    @SerializedName("Properties") var Properties:Boolean,
    @SerializedName("AllProperties") var AllProperties:Boolean
)