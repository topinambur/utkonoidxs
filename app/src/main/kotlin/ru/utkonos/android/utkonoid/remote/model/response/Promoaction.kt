package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.remote.serializer.BooleanSerializer
import java.util.ArrayList

data class Promoaction(
        var Id: String,
        var Name: String,
        var Start: String,
        var Finish: String,
        var Rules: String,
        var Description: String,
        var Image: String,
        var BannerImage: String,
        var Position: Int,
        var GoodsCount: Int,
        @SerializedName("Url") var url: String,

        @JsonAdapter(BooleanSerializer::class)
        @SerializedName("ShowParticipateButton") var showParticipateButton: Boolean,

        @SerializedName("FileRules") var fileRules: String,

        @JsonAdapter(BooleanSerializer::class)
        @SerializedName("IsParticipant") var isParticipant: Boolean,

        var actionSubcategories: ArrayList<PromoActionCategory>
)