package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddRequest(
   @SerializedName("Name") var Name:String,
   @SerializedName("Surname") var Surname:String,
   @SerializedName("Email") var Email:String,
   @SerializedName("Phone") var Phone:String,
   @SerializedName("Password") var Password:String,
   @SerializedName("SexId") var SexId:String,
   @SerializedName("SubscriptionSpecial") var SubscriptionSpecial:String
)