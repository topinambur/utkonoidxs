package ru.utkonos.android.utkonoid.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ru.utkonos.android.utkonoid.R
import ru.utkonos.android.utkonoid.ui.fragment.main.MainPageFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.content, MainPageFragment.newInstance(), MainPageFragment.javaClass.simpleName)
                    .commit()
        }
    }
}
