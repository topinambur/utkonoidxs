package ru.utkonos.android.utkonoid.ui.injection

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import ru.utkonos.android.utkonoid.ui.UtkonosApplication
import ru.utkonos.android.utkonoid.ui.injection.module.ApplicationModule
import ru.utkonos.android.utkonoid.ui.injection.module.FragmentBindingModule
import ru.utkonos.android.utkonoid.ui.injection.scopes.PerApplication

@PerApplication
@Component(modules = arrayOf(FragmentBindingModule::class, ApplicationModule::class, AndroidSupportInjectionModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

    fun inject(app: UtkonosApplication)

}