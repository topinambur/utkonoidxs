package ru.utkonos.android.utkonoid.remote.model.response

/**
 * Единица продажи
 */
data class GoodsUnit(

    /**
     * Базовые единицы измерения
     */
    var BasicUnit: String,

    /**
     * Единица продажи
     */
    var SaleUnit: String,

    /**
     * Числитель
     */
    var Numerator: Int,

    /**
     * Знаменатель
     */
    var Denominator: Int
)