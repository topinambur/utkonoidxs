package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Функционально: создание объекта «сессия пользователя».
 * Для создания сессии в части сообщения «Head» должны быть переданы поля «Username», «Password» и «DeviceId»
 */
data class UserSessionAddRequest(

   @SerializedName("DeviceId") var DeviceId:String,

   /**
    * Код для авторизации по ссылке вида https://www.utkonos.ru/feedback/:pavOrderId/:feedbackToken
     */
   @SerializedName("FeedbackToken") var FeedbackToken:String,

   /**
    * Номер заказа для авторизации по ссылке вида https://www.utkonos.ru/feedback/:pavOrderId/:feedbackToken
     */
   @SerializedName("PavOrderId") var PavOrderId:String
)