package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FaqSearchRequest(
   @SerializedName("PageId") var PageId:String
)