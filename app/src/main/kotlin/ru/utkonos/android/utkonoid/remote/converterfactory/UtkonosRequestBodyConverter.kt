package ru.utkonos.android.utkonoid.remote.converterfactory

import okhttp3.RequestBody
import java.io.IOException
import retrofit2.Converter
import java.text.SimpleDateFormat
import java.util.*

class UtkonosRequestBodyConverter<T>(private val converter: Converter<WrappedRequest<T>, RequestBody>) : Converter<WrappedRequest<T>, RequestBody> {

    @Throws(IOException::class)
    override fun convert(value: WrappedRequest<T>): RequestBody {

        val sdf = SimpleDateFormat("yyyy-dd-M hh:mm:ss")
        value.head.created = sdf.format(Date())
        value.head.requestId = UUID.randomUUID().toString()

        return converter.convert(value)
    }

}
