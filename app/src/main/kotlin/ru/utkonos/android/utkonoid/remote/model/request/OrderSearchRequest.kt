package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderSearchRequest(
   @SerializedName("Count") var Count:String,
   @SerializedName("Offset") var Offset:String,
   @SerializedName("Return") var Return:OrderSearchReturn
)

data class OrderSearchReturn(
    @SerializedName("OrderItem") var OrderItem: Boolean
)