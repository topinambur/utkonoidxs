package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsStockSubscribeRequest(
   @SerializedName("GoodsId") var GoodsId:String,
   @SerializedName("Sms") var Sms:String,
   @SerializedName("Email") var Email:String
)