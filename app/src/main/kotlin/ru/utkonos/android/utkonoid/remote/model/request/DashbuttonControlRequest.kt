package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class DashbuttonControlRequest(
   @SerializedName("ButtonGuid") var ButtonGuid:String
)