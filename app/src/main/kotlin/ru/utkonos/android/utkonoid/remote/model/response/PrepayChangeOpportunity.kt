package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Возможность изменения предоплаты
 */
data class PrepayChangeOpportunity(
    @SerializedName("CancelAllowed") var CancelAllowed: Boolean,
    @SerializedName("CancelDenyReason") var CancelDenyReason: String,
    @SerializedName("StartAllowed") var StartAllowed: Boolean,
    @SerializedName("StartDenyReason")var StartDenyReason: String
)