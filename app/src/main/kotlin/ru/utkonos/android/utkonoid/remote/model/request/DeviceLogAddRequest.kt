package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class DeviceLogAddRequest(
   @SerializedName("Data") var Data:String
)