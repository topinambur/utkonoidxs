package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Функционально: информация о кассовом чеке.
 */
data class CashvoucherSetRequest(
   @SerializedName("MasterOrderId") var MasterOrderId:String,
   @SerializedName("VoucherEmail") var VoucherEmail:String,
   @SerializedName("VoucherSms") var VoucherSms:String,
   @SerializedName("VoucherNotGetPaperCheck") var VoucherNotGetPaperCheck:String
)