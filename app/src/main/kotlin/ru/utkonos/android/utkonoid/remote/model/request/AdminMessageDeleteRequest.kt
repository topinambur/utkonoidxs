package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class AdminMessageDeleteRequest(
   @SerializedName("Id") var Id:String
)