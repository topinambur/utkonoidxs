package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Ilya Solovyov on 24.05.2018.
 * is3k@ya.ru
 */
data class FavoriteBrand (
    @SerializedName("Id") val id: String,
    @SerializedName("Name") val name: String,
    @SerializedName("ParentId") val parentId: String,
    @SerializedName("Level") val level: Int,
    @SerializedName("ImageUrl") val imageUrl: String,
    @SerializedName("HasChildren") val hasChildren: Int,
    @SerializedName("HasGoods") val hasGoods: Int
)