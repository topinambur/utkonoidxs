package ru.utkonos.android.utkonoid.remote.model.response

data class TruckTrip(
    var Id: Int,
    var StaffName: String,
    var StaffPhone: String,
    var TruckNumber: String
)