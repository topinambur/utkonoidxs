package ru.utkonos.android.utkonoid.remote.model.response

data class BrandSearchResponse(
   var BrandList : List<Brand>
) : BaseResponse()