package ru.utkonos.android.utkonoid.remote.model.response

data class User(
    var Avatar: String,
    var CardNumber: String,
    var Login: String,
    var Name: String,
    var Surname: String,
    var SexId: String,
    var BirthDate: String,
    var Email: String,
    var EmailConfirmed: Int,
    var EmailChange: String,
    var Phone: String,
    var PhoneConfirmed: Int,
    var Trusted: Int,
    var LoyalClient: Boolean
)