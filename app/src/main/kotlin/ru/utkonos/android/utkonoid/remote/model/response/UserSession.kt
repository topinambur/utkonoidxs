package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class UserSession(
    @SerializedName("Created") var created: String,
    @SerializedName("AuthToken") var authToken: String
)