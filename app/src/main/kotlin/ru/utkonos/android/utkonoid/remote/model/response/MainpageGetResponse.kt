package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class MainpageGetResponse(
    @SerializedName("GoodsSale") var GoodsSale: ArrayList<GoodsItem>,
    @SerializedName("GoodsTrend") var GoodsTrend: ArrayList<GoodsItem>,
    @SerializedName("BannerMain") var BannerMain: ArrayList<BannerMain>,
    @SerializedName("Promoactions") var Promoactions: ArrayList<Promoaction>,
    @SerializedName("DeliveryOrderForRating") var DeliveryOrderForRating: ArrayList<Order>,
    @SerializedName("Orders") var Orders: ArrayList<Order>,
    @SerializedName("GoodsOffers") var goodsOffers: ArrayList<GoodsItem>,
    @SerializedName("FavoriteBrands") var favoriteBrands: ArrayList<FavoriteBrand>,
    @SerializedName("LastComments") var lastComments: ArrayList<LastComment>,
    @SerializedName("NoveltyList")  var novelties: ArrayList<GoodsItem>,
    @SerializedName("Reviews")      var reviews: ArrayList<Review>,
    @SerializedName("ClientVersionSupportCheck")var clientVersionSupportCheck: ClientVersionSupportCheck,
    @SerializedName("OtherParams") var otherParams: OtherParams
) : BaseResponse()
