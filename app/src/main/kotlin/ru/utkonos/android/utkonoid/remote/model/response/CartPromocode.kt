package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class CartPromocode(
    @SerializedName("ActionId") var ActionId: String,
    @SerializedName("Code") var Code: String
)