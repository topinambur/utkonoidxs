package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartTemplateModifyRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("Name") var Name:String
)