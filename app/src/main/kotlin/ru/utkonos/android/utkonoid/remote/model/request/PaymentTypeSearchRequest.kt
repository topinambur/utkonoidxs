package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PaymentTypeSearchRequest(
    @SerializedName("SupportAndroidPay") var SupportAndroidPay:Boolean,
    @SerializedName("SupportApplePay") var SupportApplePay:Boolean,
    @SerializedName("SupportSamsungPay") var SupportSamsungPay:Boolean,
    @SerializedName("SupportGooglePay") var SupportGooglePay:Boolean
)