package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class CartItem(
        /**
         * Цена без скидки
         */
        @SerializedName("BasePrice") var BasePrice: String,

        /**
         * Базовые единицы измерения
         */
        @SerializedName("BasicUnit") var BasicUnit: String,

        /**
         * Код ошибки ("M" при недостатке на складе)
         */
        @SerializedName("ErrorCode") var ErrorCode: String,

        /**
         * Знаменатель измерения
         */
        @SerializedName("Denominator") var Denominator: Int,

        /**
         * ПК товара
         */
        @SerializedName("GoodsItemId") var GoodsItemId: String,

        /**
         * Ссылка на малое изображение
         */
        @SerializedName("ImageSmallUrl") var ImageSmallUrl: String,

        /**
         * Ссылка на большое изображение
         */
        @SerializedName("ImageBigUrl") var ImageBigUrl: String,

        /**
         * Ссылка на изображение высокого разрешения
         */
        @SerializedName("ImageHighUrl") var ImageHighUrl: String,

        /**
         * Остаток на складе
         */
        @SerializedName("InStockCount") var InStockCount: Int,

        /**
         * Название
         */
        @SerializedName("Name") var Name: String,

        /**
         *
         */
        @SerializedName("MinSaleQuantity") var MinSaleQuantity: String,

        /**
         *
         */
        @SerializedName("MaxSaleQuantity") var MaxSaleQuantity: String,

        /**
         * Числитель измерения
         */
        @SerializedName("Numerator") var Numerator: Int,

        /**
         * Цена
         * конечная цена за единицу продажи, а не за базовую единицу измерения как в каталоге.
         */
        @SerializedName("Price") var Price: String,

        /**
         * Цена за позицию
         * цена за позицию заказа, в общем случае вычисляется как Price*Quantity, но при наличии скидки значение может отличаться.
         */
        @SerializedName("PriceTotal") var PriceTotal: String,

        /**
         * Количество в корзине
         */
        @SerializedName("Quantity") var Quantity: Int,

        /**
         * Единица продажи
         */
        @SerializedName("SaleUnit") var SaleUnit: String
)