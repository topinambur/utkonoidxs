package ru.utkonos.android.utkonoid.remote.converterfactory

import ru.utkonos.android.utkonoid.data.model.UtkonosError
class UtkonosAPIException( utkonosError: UtkonosError) : Exception(utkonosError.Message) {
}
