package ru.utkonos.android.utkonoid.remote.model.response

data class PartnerAction(
    var Id: String,
    var Name: String,
    var Text: String,
    var Url: String,
    var UrlRegister: String,
    var IdentifierMask: String,
    var Placeholder: String,
    var Image: String,
    var XorGroup: String,
    var XorEnabled: Int,
    var Sort: Int,
    var Identifier: String
)