package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CommentSearchRequest(
   @SerializedName("GoodsItemId") var GoodsItemId:String,
   @SerializedName("ParentId") var ParentId:String
)