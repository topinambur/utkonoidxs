package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PromoactionSearchRequest(
    @SerializedName("Id") var Id:String,
    @SerializedName("PromoactionCategoryId") var PromoactionCategoryId:String,
    @SerializedName("Count") var Count:String,
    @SerializedName("Offset") var Offset:String,
    @SerializedName("Return") var Return:ReturnPromoactionSearch
)

data class ReturnPromoactionSearch(
    @SerializedName("PromoactionCategoryList") var PromoactionCategoryList: String,
    @SerializedName("PromoactionGoodsList") var PromoactionGoodsList: String,
    @SerializedName("PromoactionGoodsItemList") var PromoactionGoodsItemList: String
)