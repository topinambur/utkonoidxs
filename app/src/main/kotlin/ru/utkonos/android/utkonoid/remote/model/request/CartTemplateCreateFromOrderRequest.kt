package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartTemplateCreateFromOrderRequest(
   @SerializedName("Name") var Name:String,
   @SerializedName("OrderId") var OrderId:String
)