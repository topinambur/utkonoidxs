package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class RecipeCommentAddRequest(
    @SerializedName("RecipeId") var RecipeId: String,
    @SerializedName("Text") var Text: String,
    @SerializedName("Rating") var Rating: String
)

