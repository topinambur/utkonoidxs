package ru.utkonos.android.utkonoid.data

import io.reactivex.Single
import ru.utkonos.android.utkonoid.data.source.UtkonosDataStoreFactory
import ru.utkonos.android.utkonoid.domain.repository.UtkonosRepository
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import javax.inject.Inject

/**
 * Provides an implementation of the [UtkonosRemote] interface for communicating to and from
 * data sources
 */
class UtkonosDataRepository @Inject constructor(private val factory: UtkonosDataStoreFactory) : UtkonosRepository {

    //override fun clearMessages(): Completable {
    //    return factory.retrieveCacheDataStore().clearMessages()
    //}

    //override fun saveMessage(list: List<AdminMessage>): Completable {
    //    val messageEntities = list.map { messageMapper.mapToEntity(it) }
    //    return saveMessageEntities(messageEntities)
    //}

    //private fun saveAdminMessageEntities(list: List<AdminMessageListEntity>): Completable {
    //    return factory.retrieveCacheDataStore().saveAdminMessageList(list)
    //}

    override fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse> {
        val dataStore = factory.retrieveDataStore()
        return dataStore.getMainPage(body)
                //.map { list ->
                    //list.map { listItem ->
                //        utkonosMapper.mapFromEntity(list)
                    //}
                //}
    }

}