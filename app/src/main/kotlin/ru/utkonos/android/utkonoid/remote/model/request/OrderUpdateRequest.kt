package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderUpdateRequest(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("GoodsCountList") var GoodsCountList:List<GoodsCountItem>
)

data class GoodsCountItem(
    @SerializedName("GoodsItemId") var GoodsItemId: String,
    @SerializedName("Count") var Count: String
)