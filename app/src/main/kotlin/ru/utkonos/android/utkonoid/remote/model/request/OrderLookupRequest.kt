package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderLookupRequest(
    @SerializedName("Id") var Id:String,
    @SerializedName("SupportApplePay") var SupportApplePay:Boolean = false,
    @SerializedName("SupportAndroidPay") var SupportAndroidPay:Boolean = false,
    @SerializedName("SupportSamsungPay") var SupportSamsungPay:Boolean = false,
    @SerializedName("SupportGooglePay") var SupportGooglePay:Boolean = false,
    @SerializedName("Return") var Return:OrderLookupReturn
)

data class OrderLookupReturn(
    @SerializedName("Address") var Address: Boolean,
    @SerializedName("AvailablePaymentTypes") var AvailablePaymentTypes: Boolean,
    @SerializedName("Goods") var Goods: Boolean,
    @SerializedName("GoodsItem") var GoodsItem: Boolean,
    @SerializedName("OrderItem") var OrderItem: Boolean,
    @SerializedName("Substitutes") var Substitutes: Boolean,
    @SerializedName("TripPath") var TripPath: Boolean,
    @SerializedName("User") var User: Boolean
)