package ru.utkonos.android.utkonoid.remote.model.response

data class Rating(
    var Rate: Int,
    var Votes: Int,
    var MyRate: Int
)