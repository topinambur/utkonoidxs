package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteCategorySearchRequest(
    @SerializedName("Deep") var Deep:String,
    @SerializedName("Id") var Id:String,
    @SerializedName("OnlyTopMarkings") var OnlyTopMarkings:String,
    @SerializedName("ParentId") var ParentId:String,
    @SerializedName("Return") var Return: FavoriteCategorySearchReturn
)

data class FavoriteCategorySearchReturn(
    @SerializedName("FavoriteItem") var FavoriteItem:Boolean,
    @SerializedName("GoodsItem") var GoodsItem:Boolean
)