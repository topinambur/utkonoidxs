package ru.utkonos.android.utkonoid.remote.model.response

data class UserAddress(

    var Id: String,

    @Deprecated("")
    var TownId: String,

    /**
     * Подмосковье/Москва
     */
    var TownTitle: String,


    @Deprecated("")
    var DistrictId: String,

    var HouseNumber: String,
    var Editable: Boolean,
    var FlatNumber: String,
    var CityTitle: String,
    var StreetTitle: String,
    var HouseFrame: String,
    var HouseBuilding: String,
    var EntranceNumber: String,
    var FlatFloor: String,

    @Deprecated("")
    var Phone: String,

    var Information: String,
    var YandexLatitude: String,
    var YandexLongitude: String,
    var IsVacationHome: Boolean,
    var IsOffice: Boolean
)