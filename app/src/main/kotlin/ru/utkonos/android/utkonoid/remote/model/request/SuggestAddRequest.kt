package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SuggestAddRequest(
    @SerializedName("TypeId") var TypeId:String,
    @SerializedName("Email") var Email:String,
    @SerializedName("OrderId") var OrderId:String,
    @SerializedName("Phone") var Phone:String,
    @SerializedName("Name") var Name:String,
    @SerializedName("Message") var Message:String
)