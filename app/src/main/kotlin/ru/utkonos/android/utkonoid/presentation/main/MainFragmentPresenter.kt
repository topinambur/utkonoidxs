package ru.utkonos.android.utkonoid.presentation.main

import io.reactivex.observers.DisposableSingleObserver
import ru.utkonos.android.utkonoid.domain.interactor.SingleUseCase
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import javax.inject.Inject

class MainFragmentPresenter @Inject constructor(val mainPageFragment: MainFragmentContract.View,
                                                val getMainPageUseCase: SingleUseCase<MainpageGetResponse, Void>
):
        MainFragmentContract.Presenter {

    init {
        mainPageFragment.setPresenter(this)
    }

    override fun start() {
        retrieveMainPage()
    }

    override fun stop() {
        getMainPageUseCase.dispose()
    }

    override fun retrieveMainPage() {
        getMainPageUseCase.execute(MainpageGetSubscriber())
    }

    internal fun handleGetMainPageSuccess(data: MainpageGetResponse) {
        mainPageFragment.hideErrorState()
        mainPageFragment.hideEmptyState()
        mainPageFragment.refreshMainPage( data )
    }

    inner class MainpageGetSubscriber: DisposableSingleObserver<MainpageGetResponse>() {

        override fun onSuccess(t: MainpageGetResponse) {
            handleGetMainPageSuccess(t)
        }

        override fun onError(exception: Throwable) {
            mainPageFragment.hideEmptyState()
            mainPageFragment.showErrorState()
        }

    }

}