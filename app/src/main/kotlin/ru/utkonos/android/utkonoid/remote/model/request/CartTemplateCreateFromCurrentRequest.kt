package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartTemplateCreateFromCurrentRequest(
   @SerializedName("Name") var Name:String
)