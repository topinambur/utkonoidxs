package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class UserSettingsResponse(
    @SerializedName("UserSettingsList") var UserSettingsList: List<UserSettings>
) : BaseResponse()