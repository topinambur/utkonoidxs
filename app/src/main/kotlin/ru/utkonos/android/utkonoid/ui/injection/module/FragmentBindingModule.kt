package ru.utkonos.android.utkonoid.ui.injection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.utkonos.android.utkonoid.ui.fragment.main.MainPageFragment
import ru.utkonos.android.utkonoid.ui.injection.scopes.PerFragment

@Module
abstract class FragmentBindingModule {

    @PerFragment
    @ContributesAndroidInjector(modules = arrayOf(MainFragmentModule::class))
    abstract fun bindMainPageFragment(): MainPageFragment

}