package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class EventsSaveRequest(
   @SerializedName("Time") var Time:String,
   @SerializedName("Events") var Events:String
)