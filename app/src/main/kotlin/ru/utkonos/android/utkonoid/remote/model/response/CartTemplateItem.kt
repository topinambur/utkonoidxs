package ru.utkonos.android.utkonoid.remote.model.response

data class CartTemplateItem(
    var Id: String,
    var CartTemplateId: String,
    var GoodsItemId: String,
    var Count: Int
)