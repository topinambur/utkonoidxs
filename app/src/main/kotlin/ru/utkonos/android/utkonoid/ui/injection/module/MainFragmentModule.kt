package ru.utkonos.android.utkonoid.ui.injection.module

import dagger.Module
import dagger.Provides
import ru.utkonos.android.utkonoid.domain.interactor.main.MainPageGet
import ru.utkonos.android.utkonoid.presentation.main.MainFragmentContract
import ru.utkonos.android.utkonoid.presentation.main.MainFragmentPresenter
import ru.utkonos.android.utkonoid.ui.fragment.main.MainPageFragment
import ru.utkonos.android.utkonoid.ui.injection.scopes.PerFragment


/**
 * Module used to provide dependencies at an fragment-level.
 */
@Module
open class MainFragmentModule {

    @PerFragment
    @Provides
    internal fun provideMainPageView(mainPageFragment: MainPageFragment): MainFragmentContract.View {
        return mainPageFragment
    }

    @PerFragment
    @Provides
    internal fun provideMainFragmentPresenter(mainView: MainFragmentContract.View,
                                              getMainPage: MainPageGet):
            MainFragmentContract.Presenter {
        return MainFragmentPresenter(mainView, getMainPage)
    }

}