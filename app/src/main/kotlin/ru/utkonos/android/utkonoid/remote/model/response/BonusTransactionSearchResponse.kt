package ru.utkonos.android.utkonoid.remote.model.response

data class BonusTransactionSearchResponse(
    var BonusTransactionList: List<BonusTransaction>
) : BaseResponse()