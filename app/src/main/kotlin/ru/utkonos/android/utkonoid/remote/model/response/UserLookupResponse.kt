package ru.utkonos.android.utkonoid.remote.model.response

data class UserLookupResponse(
    var UserList: List<User>,
    var BonusAccountList: List<BonusAccount>
) : BaseResponse()