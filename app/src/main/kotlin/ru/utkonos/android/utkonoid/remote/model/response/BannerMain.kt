package ru.utkonos.android.utkonoid.remote.model.response

data class BannerMain(
    var Id: Int,
    var Image: String,
    var Href: String,
    var Rating: Int,
    var Title: String,
    var GoodsCategoryId: String,
    var GoodsItemId: String,
    var PromoactionId: String,
    var NewsMessageId: String
)