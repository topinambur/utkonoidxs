package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CommentRatingRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("Rating") var Rating:String
)