package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class AdminMessage (
    @SerializedName("Id") var Id: String,
    @SerializedName("ParentId") var ParentId: String,
    @SerializedName("FromAdmin") var FromAdmin: Int,
    @SerializedName("AdminName") var AdminName: String,
    @SerializedName("Message") var Message: String,
    @SerializedName("Date") var Date: String,
    @SerializedName("MessageHtml") var MessageHtml: String
)