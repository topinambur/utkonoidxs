package ru.utkonos.android.utkonoid.remote.model.response

/**
 * Расширенное описание материала
 */
data class GoodsDescription(

    /**
     * Идентификатор описания
     */
    var Id: Int,

    /**
     * Идентификатор материала
     */
    var GoodsItemId: String,

    /**
     * Идентификатор типа
     */
    var GoodsDescriptionTypeId: Int,

    /**
     * Описание
     */
    var Description: String
)