package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressDeliveryIntervalSearch(
    @SerializedName("UserAddressId") var UserAddressId: String,
    @SerializedName("Return") var Return: UserAddressDeliveryIntervalSearchReturn
)

data class UserAddressDeliveryIntervalSearchReturn(
    @SerializedName("Disabled") var Disabled: Boolean
)
