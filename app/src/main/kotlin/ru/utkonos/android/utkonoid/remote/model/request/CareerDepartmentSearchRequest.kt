package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CareerDepartmentSearchRequest(
        @SerializedName("DepartmentId") var DepartmentId:String,
        @SerializedName("Return") var Return:CareerDepartmentSearchReturn
)

data class CareerDepartmentSearchReturn(
        @SerializedName("Vacancies") var Vacancies: Boolean = true
)