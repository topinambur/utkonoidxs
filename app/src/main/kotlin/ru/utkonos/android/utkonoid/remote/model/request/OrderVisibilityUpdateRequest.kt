package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderVisibilityUpdateRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("Hidden") var Hidden:String
)