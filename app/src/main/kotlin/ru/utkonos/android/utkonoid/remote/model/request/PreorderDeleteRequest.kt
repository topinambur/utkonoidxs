package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PreorderDeleteRequest(
    @SerializedName("PreorderId") var PreorderId:String
)
