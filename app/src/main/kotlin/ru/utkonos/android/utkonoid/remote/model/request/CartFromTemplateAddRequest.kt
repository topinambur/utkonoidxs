package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartFromTemplateAddRequest(
    @SerializedName("CartTemplateId") var CartTemplateId:String,
    @SerializedName("Return")  var Return:CartFromTemplateAddReturn
)

data class CartFromTemplateAddReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)