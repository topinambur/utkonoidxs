package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class AdminMessageReplyRequest(
   @SerializedName("ParentId") var ParentId:String,
   @SerializedName("Message") var Id:String
)