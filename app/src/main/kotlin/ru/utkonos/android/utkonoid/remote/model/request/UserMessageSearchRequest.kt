package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserMessageSearchRequest(
        @SerializedName("Count") var Count:String,
        @SerializedName("Id") var Id:String,
        @SerializedName("Offset") var Offset:String,
        @SerializedName("Return") var Return:UserMessageSearchReturn
)

data class UserMessageSearchReturn(
        @SerializedName("Goods") var Goods: Boolean,
        @SerializedName("JsonHref") var JsonHref: Boolean,
        @SerializedName("Plain") var Plain: Boolean,
        @SerializedName("Short") var Short: Boolean,
        @SerializedName("Text") var Text: Boolean,
        @SerializedName("TextFilter") var TextFilter: Boolean
)
