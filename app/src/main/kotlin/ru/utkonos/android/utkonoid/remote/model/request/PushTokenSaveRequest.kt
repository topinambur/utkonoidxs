package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PushTokenSaveRequest(
   @SerializedName("Type") var Type:String,
   @SerializedName("Token") var Token:String
)