package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class IntervalFixeRequest(
   @SerializedName("IntervalId") var IntervalId:String,
   @SerializedName("Cost") var Cost:String
)