package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SocialNetworkJoinRequest(
    @SerializedName("SocialNetworkId") var SocialNetworkId:String,
    @SerializedName("Bind") var Bind:String,
    @SerializedName("FinishType") var FinishType:String
)