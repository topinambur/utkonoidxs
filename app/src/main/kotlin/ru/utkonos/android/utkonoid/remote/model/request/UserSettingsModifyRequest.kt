package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserSettingsModifyRequest(
   @SerializedName("SubscriptionSpecial") var SubscriptionSpecial:String,
   @SerializedName("SubscriptionSpecialSms") var SubscriptionSpecialSms:String,
   @SerializedName("SubscriptionOrderAdd") var SubscriptionOrderAdd:String,
   @SerializedName("SubscriptionOrderStatus") var SubscriptionOrderStatus:String,
   @SerializedName("SubscriptionOrderNotifySms") var SubscriptionOrderNotifySms:String,
   @SerializedName("SubscriptionBonusNotifySms") var SubscriptionBonusNotifySms	:String,
   @SerializedName("ViberSpecialOffers") var ViberSpecialOffers:String,
   @SerializedName("ShowAbsentGoods") var ShowAbsentGoods:String
)