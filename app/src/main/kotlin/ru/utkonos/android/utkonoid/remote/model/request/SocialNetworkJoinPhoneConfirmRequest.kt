package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SocialNetworkJoinPhoneConfirmRequest(
    @SerializedName("Phone") var Phone:String,
    @SerializedName("Code") var Code:String
)