package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderAddRequest(
   @SerializedName("PaymentCancelUrl") var PaymentCancelUrl:String,
   @SerializedName("PaymentSuccessUrl") var PaymentSuccessUrl:String,
   @SerializedName("PaymentTypeId") var PaymentTypeId:String,
   @SerializedName("PaymentTypeQiwiPhone") var PaymentTypeQiwiPhone:String
)