package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class RatingAddRequest(
    @SerializedName("GoodsItemId") var GoodsItemId: String,
    @SerializedName("Rating") var Rating: String
)

