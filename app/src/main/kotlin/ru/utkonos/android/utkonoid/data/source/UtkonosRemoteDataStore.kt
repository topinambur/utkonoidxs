package ru.utkonos.android.utkonoid.data.source

import io.reactivex.Single
import ru.utkonos.android.utkonoid.data.repository.UtkonosDataStore
import ru.utkonos.android.utkonoid.data.repository.UtkonosRemote
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import javax.inject.Inject

/**
 * Implementation of the [UtkonosDataStore] interface to provide a means of communicating
 * with the remote data source
 */
open class UtkonosRemoteDataStore @Inject constructor(private val utkonosRemote: UtkonosRemote) : UtkonosDataStore {

    override fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse> {
        return utkonosRemote.getMainPage(body)
    }
}