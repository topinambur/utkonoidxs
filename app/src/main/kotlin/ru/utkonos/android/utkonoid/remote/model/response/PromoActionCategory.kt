package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class PromoActionCategory(

        /**
         * Идентификатор
         */
        @SerializedName("Id") var Id: String,

        /**
         * Промо-акция, к которой относится раздел
         */
        @SerializedName("PromoactionId") var PromoactionId: Int,

        /**
         * Родительский раздел
         */
        @SerializedName("ParentId") var ParentId: Int,

        /**
         * Название раздела
         */
        @SerializedName("Name") var Name:String,

        /**
         * Порядок сортировки внутри родительского раздела
         */
        @SerializedName("Position") var Position: Int,

        /**
         * Количество материалов по акции (в наличии)
         */
        @SerializedName("GoodsCount") var GoodsCount: Int

)