package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import ru.utkonos.android.utkonoid.data.model.UtkonosError

open class BaseResponse {
    @SerializedName("ErrorList")
    lateinit var ErrorList : ArrayList<UtkonosError>
}