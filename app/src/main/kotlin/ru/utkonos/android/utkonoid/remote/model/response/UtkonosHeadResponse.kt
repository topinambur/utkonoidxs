package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Representation for a [UtkonosModel] fetched from the API
 */
class UtkonosHeadResponse(
      @SerializedName("RequestId")     val requestId     : String
    , @SerializedName("Created")       val created       : String
    , @SerializedName("Method")        val method        : String
    , @SerializedName("ServerIp")      val serverIp      : String
    , @SerializedName("Status")        val status        : String
    , @SerializedName("SessionToken")  val sessionToken  : String
    , @SerializedName("SessionGroups") val sessionGroups : Array<String>
)