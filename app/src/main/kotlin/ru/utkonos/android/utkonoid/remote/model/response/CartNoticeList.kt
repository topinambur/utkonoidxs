package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Сообщение корзины.
 * Информационные сообщения, ошибки, которые препятствуют оформлению заказа.
 */
data class CartNoticeList(
    /**
     * Идентификатор исключения
     */
    @SerializedName("Id") var Id: Int,

    /**
     * Класс исключения
     */
    @SerializedName("Class") var Class: String,

    /**
     * Текст ошибки для пользователя
     */
    @SerializedName("Description") var Description: String,

    /**
     * Идентификатор типа исключения
     */
    @SerializedName("TypeId") var TypeId: Int
)