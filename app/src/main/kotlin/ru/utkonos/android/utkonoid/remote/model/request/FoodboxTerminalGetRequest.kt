package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FoodboxTerminalGetRequest(
   @SerializedName("TerminalId") var TerminalId:String,
   @SerializedName("TerminalTypeId") var TerminalTypeId:String,
   @SerializedName("SeparateFoodboxErrors") var SeparateFoodboxErrors:String
)