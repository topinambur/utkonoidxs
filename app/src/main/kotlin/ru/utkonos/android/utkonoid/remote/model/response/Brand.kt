package ru.utkonos.android.utkonoid.remote.model.response

data class Brand(
    var Id: String,
    var Name: String
)