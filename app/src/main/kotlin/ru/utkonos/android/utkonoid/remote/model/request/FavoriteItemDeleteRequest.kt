package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteItemDeleteRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("GoodsItemId") var GoodsItemId:String
)