package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressDeliveryIntervalExternalSearchRequest(
    @SerializedName("CartId") var CartId: String,
    @SerializedName("UserAddress") var UserAddress: UserAddressData
)

