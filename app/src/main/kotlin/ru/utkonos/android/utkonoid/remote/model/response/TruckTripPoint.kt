package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.JsonAdapter
import ru.utkonos.android.utkonoid.remote.serializer.BooleanSerializer

/**
 * Данные о контрольных точках (заказах) на маршруте доставки возвращаются вместе с информацией по заказу методе orderLookup.
 */
data class TruckTripPoint(

        /**
         * Идентификатор точки
         */
        var Id: Int,

        /**
         * Идентификатор рейса TruckTrip
         */
        var TruckTripId: Int,
        var PointNumber: Int,
        var Longitude: Float,
        var Latitude: Float,

        @JsonAdapter(BooleanSerializer::class)
        var Arrive: Boolean = false,

        /**
         * Факт убытия с точки
         */
        @JsonAdapter(BooleanSerializer::class)
        var Departure: Boolean,

        /**
         * Отмена заказа
         */
        @JsonAdapter(BooleanSerializer::class)
        var Cancel: Boolean,

        /**
         * Идентификатор заказа Order. Передается только для своего заказа.
         */
        var OrderId: String
)