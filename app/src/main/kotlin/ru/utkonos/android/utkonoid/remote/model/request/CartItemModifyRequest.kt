package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemModifyRequest(
    @SerializedName("GoodsItemId") var GoodsItemId:String,
    @SerializedName("Quantity") var Quantity:String,
    @SerializedName("Source")  var Source:String,
    @SerializedName("SourceId")  var SourceId:String,
    @SerializedName("Return")  var Return: CartItemModifyReturn
)

data class CartItemModifyReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)