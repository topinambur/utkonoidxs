package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ResponseLookupRequest(
    @SerializedName("PendingRequestKey") var PendingRequestKey:String
)