package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressModifyRequest(
    @SerializedName("UserAddress") var UserAddress: UserAddressData
)

data class UserAddressData(
    @SerializedName("DistrictId") var DistrictId: String,
    @SerializedName("EntranceNumber") var EntranceNumber: String,
    @SerializedName("FlatFloor") var FlatFloor: String,
    @SerializedName("FlatNumber") var FlatNumber: String,
    @SerializedName("HouseBuilding") var HouseBuilding: String,
    @SerializedName("HouseFrame") var HouseFrame: String,
    @SerializedName("HouseNumber") var HouseNumber: String,
    @SerializedName("Id") var Id: String,
    @SerializedName("Information") var Information: String,
    @SerializedName("IsVacationHome") var IsVacationHome: Boolean = false,
    @SerializedName("IsOffice") var IsOffice: Boolean = false,
    @SerializedName("Phone") var Phone: Boolean = false,
    @SerializedName("StreetTitle") var StreetTitle: String,
    @SerializedName("TownId") var TownId: String,
    @SerializedName("TownTitle") var TownTitle: String,
    @SerializedName("YandexLatitude") var YandexLatitude: String,
    @SerializedName("YandexLongitude") var YandexLongitude: String
)
