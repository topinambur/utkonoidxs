package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ScanHistoryDeleteRequest(
    @SerializedName("GoodsItemId") var GoodsItemId:String
)