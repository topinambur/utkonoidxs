package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FoodboxTerminalSearchRequest(
   @SerializedName("SeparateFoodboxErrors") var SeparateFoodboxErrors:String,
   @SerializedName("AddPickpointTerminals") var AddPickpointTerminals:String,
   @SerializedName("FavoriteOnly") var FavoriteOnly:String

)