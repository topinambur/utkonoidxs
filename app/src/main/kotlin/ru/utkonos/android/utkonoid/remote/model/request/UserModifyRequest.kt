package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserModifyRequest(
   @SerializedName("Name") var Name:String,
   @SerializedName("Surname") var Surname:String,
   @SerializedName("SexId") var SexId:String,
   @SerializedName("Email") var Email:String,
   @SerializedName("Birthday") var Birthday:String
)