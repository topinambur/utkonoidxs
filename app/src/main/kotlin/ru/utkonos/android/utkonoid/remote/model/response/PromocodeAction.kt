package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class PromocodeAction(
    /**
     * Код акции
     */
    @SerializedName("Id") var Id: String,
    /**
     * Размер скидки
     */
    @SerializedName("Discount") var Discount: String,
    /**
     * Тип скидки: "Procent" - процентная (процент от стоимости товаров), "Absolute" - абсолютная (фиксированная)
     */
    @SerializedName("BonusType") var DiscountType: String,
    /**
     * Бесплатная доставка
     */
    @SerializedName("FreeDelivery") var FreeDelivery: String,
    @SerializedName("ActionType") var ActionType: String,
    /**
     * Минимальная стоимость товаров для использования промо-кода.
     */
    @SerializedName("MinCost") var MinCost: String
)