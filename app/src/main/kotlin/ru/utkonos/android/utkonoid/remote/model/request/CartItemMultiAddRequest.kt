package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemMultiAddRequest(
        @SerializedName("GoodsItemId") var GoodsItemId:String,
        @SerializedName("Count") var Count:String,
        @SerializedName("Source")  var Source:String,
        @SerializedName("SourceId")  var SourceId:String,
        @SerializedName("Return")  var Return: CartItemMultiAddReturn
)

data class CartItemMultiAddReturn(
        @SerializedName("Cart") var Cart: Boolean,
        @SerializedName("CartItemList") var CartItemList:Boolean,
        @SerializedName("TotalCost") var TotalCost:Boolean
)