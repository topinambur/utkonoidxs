package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemAddRequest(
    @SerializedName("GoodsItemId") var GoodsItemId:String,
    @SerializedName("Quantity") var Quantity:String,
    @SerializedName("Source") var Source:String,
    @SerializedName("SourceId") var SourceId:String,
    @SerializedName("Return")  var Return: CartItemAddReturn
)

data class CartItemAddReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)