package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ClientVersionSupportCheck(
   @SerializedName("Client") var Client:String
)