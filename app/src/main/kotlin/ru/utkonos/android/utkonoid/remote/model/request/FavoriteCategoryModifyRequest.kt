package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FavoriteCategoryModifyRequest(
   @SerializedName("Id") var Id:String,
   @SerializedName("Title") var Title:String,
   @SerializedName("ParentId") var ParentId:String
)