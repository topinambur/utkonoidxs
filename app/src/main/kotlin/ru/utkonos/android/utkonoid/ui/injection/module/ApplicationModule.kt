package ru.utkonos.android.utkonoid.ui.injection.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.utkonos.android.utkonoid.BuildConfig
import ru.utkonos.android.utkonoid.data.UtkonosDataRepository
import ru.utkonos.android.utkonoid.data.executor.JobExecutor
import ru.utkonos.android.utkonoid.data.repository.UtkonosRemote
import ru.utkonos.android.utkonoid.data.source.UtkonosDataStoreFactory
import ru.utkonos.android.utkonoid.domain.executor.PostExecutionThread
import ru.utkonos.android.utkonoid.domain.executor.ThreadExecutor
import ru.utkonos.android.utkonoid.domain.repository.UtkonosRepository
import ru.utkonos.android.utkonoid.remote.UtkonosRemoteImpl
import ru.utkonos.android.utkonoid.remote.UtkonosService
import ru.utkonos.android.utkonoid.remote.UtkonosServiceFactory
import ru.utkonos.android.utkonoid.ui.UiThread
import ru.utkonos.android.utkonoid.ui.injection.scopes.PerApplication

/**
 * Module used to provide dependencies at an application-level.
 */
@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    internal fun provideUtkonosRepository(factory: UtkonosDataStoreFactory): UtkonosRepository {
        return UtkonosDataRepository(factory)
    }

    @Provides
    @PerApplication
    internal fun provideUtkonosRemote(service: UtkonosService): UtkonosRemote {
        // TODO: Получать здесь token, marketingKey и DeviceId
        /*var deviceId = application.applicationContext().settingsManager.getString(SettingsManager.DEVICE_ID, null)
        if (deviceId == null) {
            deviceId = "A-" + UUID.randomUUID().toString()
            settingsManager.setString(SettingsManager.DEVICE_ID, deviceId)
        }*/

        return UtkonosRemoteImpl(service, token="", marketingKey="123123123", deviceId="1537417406")
    }

    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    internal fun provideUtkonosService(): UtkonosService {
        return UtkonosServiceFactory.makeUtkonosService(BuildConfig.DEBUG)
    }
}