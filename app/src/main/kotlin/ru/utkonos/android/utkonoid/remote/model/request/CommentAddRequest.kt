package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CommentAddRequest(
   @SerializedName("GoodsItemId") var GoodsItemId:String,
   @SerializedName("ParentId") var ParentId:String,
   @SerializedName("Text") var Text:String
)