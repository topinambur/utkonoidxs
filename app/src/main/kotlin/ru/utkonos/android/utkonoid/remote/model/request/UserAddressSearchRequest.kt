package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressSearchRequest(
    @SerializedName("Offset") var Offset:String,
    @SerializedName("Count") var Count:String
)
