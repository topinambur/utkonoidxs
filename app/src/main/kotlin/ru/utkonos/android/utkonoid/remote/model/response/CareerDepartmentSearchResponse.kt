package ru.utkonos.android.utkonoid.remote.model.response

import ru.utkonos.android.utkonoid.remote.model.Department

data class CareerDepartmentSearchResponse(
   var DepartmentList : List<Department>
) : BaseResponse()