package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class RegisterResponse(
    @SerializedName("UserList") var UserList: ArrayList<UserListItem>
)

data class UserListItem(
    @SerializedName("CardNumber") var cardNumber: String,
    @SerializedName("Login") var login: String,
    @SerializedName("Password") var password: String
)