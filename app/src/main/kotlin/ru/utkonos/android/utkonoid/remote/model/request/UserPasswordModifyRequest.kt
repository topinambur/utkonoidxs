package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserPasswordModifyRequest(
   @SerializedName("Current") var Current:String,
   @SerializedName("New") var New:String,
   @SerializedName("Confirm") var Confirm:String
)