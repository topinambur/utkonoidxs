package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ScanHistoryAddRequest(
    @SerializedName("GoodsItemId") var GoodsItemId:String
)