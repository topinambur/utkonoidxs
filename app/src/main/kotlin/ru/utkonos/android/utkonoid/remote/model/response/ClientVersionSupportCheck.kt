package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Ilya Solovyov on 04.06.2018.
 * is3k@ya.ru
 */
data class ClientVersionSupportCheck(
        @SerializedName("Id") val id: String,
        @SerializedName("Updated") val updated: String,
        @SerializedName("Version") val version: String,
        @SerializedName("Description") val description: String,
        @SerializedName("ShowModal") val showModal: Boolean
)