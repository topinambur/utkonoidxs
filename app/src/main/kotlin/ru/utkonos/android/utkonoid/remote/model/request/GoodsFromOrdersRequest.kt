package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsFromOrdersRequest(
   @SerializedName("CountLastOrders") var CountLastOrders:String
)