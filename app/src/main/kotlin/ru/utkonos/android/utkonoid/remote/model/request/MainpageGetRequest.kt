package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Запрос баннеров для главного экрана.
 */
data class MainpageGetRequest(
   @SerializedName("BannerChannel") var BannerChannel:String = "portable",
   @SerializedName("Client") var Client:String = "Android",
   @SerializedName("Return") var ReturnMainpage:MainpageGetReturn = MainpageGetReturn()
)

data class MainpageGetReturn(
        @SerializedName("Cart") var Cart: Boolean = false,
        @SerializedName("Orders") var Orders: Boolean = true,
        @SerializedName("Banners") var Banners: Boolean = true,
        @SerializedName("Categories") var Categories: Boolean = false,
        @SerializedName("GoodsSale") var GoodsSale: Boolean = true,
        @SerializedName("GoodsTrend") var GoodsTrend: Boolean = true,
        @SerializedName("CartGoods") var CartGoods: Boolean = false,

        /**
         * Возвращать товары-лидеры ("Наше предложение")
         */
        @SerializedName("GoodsOffers") var GoodsOffers: Boolean = true,
        @SerializedName("DeliveryOrderForRating") var DeliveryOrderForRating: Boolean = true,
        @SerializedName("PromoAction") var PromoAction: Boolean = true,
        @SerializedName("Specials") var Specials: Boolean = false,
        @SerializedName("FavoriteBrands") var FavoriteBrands: Boolean = true,
        @SerializedName("LastComments") var LastComments: Boolean = true,
        @SerializedName("Reviews") var Reviews: Boolean = true,
        @SerializedName("BuyersChoice") var BuyersChoice: Boolean = true,
        @SerializedName("Novelty") var Novelty: Boolean = true
)