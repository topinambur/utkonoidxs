package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class ScanHistorySearchRequest(
    @SerializedName("Return") var Return:ReturnScanHistorySearch,
    @SerializedName("Limit") var Limit:LimitScanHistorySearch
)

data class ReturnScanHistorySearch(
    @SerializedName("ScanHistoryItem") var ScanHistoryItem: String,
    @SerializedName("GoodsItem") var GoodsItem: String
)

data class LimitScanHistorySearch(
    @SerializedName("Count") var Count: String,
    @SerializedName("Offset") var Offset: String
)