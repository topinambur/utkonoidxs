package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartTemplateSearchRequest(
    @SerializedName("Id") var Id:String,
    @SerializedName("Return") var Return: CartTemplateSearchReturn
)

data class CartTemplateSearchReturn(
    @SerializedName("CartTemplateItemList") var CartTemplateItemList:Boolean,
    @SerializedName("GoodsItemList") var GoodsItemList:Boolean
)