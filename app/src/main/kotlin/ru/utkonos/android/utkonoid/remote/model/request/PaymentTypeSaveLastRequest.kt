package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class PaymentTypeSaveLastRequest(
    @SerializedName("PaymentTypeId") var PaymentTypeId:String
)
