package ru.utkonos.android.utkonoid.remote.converterfactory

import com.google.gson.annotations.SerializedName

import ru.utkonos.android.utkonoid.remote.model.response.UtkonosHeadResponse

/**
 * Завернутый ответ от сервера, содержащий заголовок Head и Body указанного в интерфейсе retrofit типа
 */
class WrappedResponse<T>(
    @SerializedName("Head") var head: UtkonosHeadResponse,
    @SerializedName("Body") var body: T
)