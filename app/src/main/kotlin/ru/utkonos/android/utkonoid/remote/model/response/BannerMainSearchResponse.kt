package ru.utkonos.android.utkonoid.remote.model.response

data class BannerMainSearchResponse(
    var BannerMainList:List<BannerMain>
) : BaseResponse()