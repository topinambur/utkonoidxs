package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

/**
 * Добавление в корзину ингредиентов из рецепта.
 */
data class CartChangeRecipeRequest(
    @SerializedName("RecipeId") var RecipeId:String
)