package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class SendSmsAuthCodeRequest(
    @SerializedName("Phone") var Phone:String
)