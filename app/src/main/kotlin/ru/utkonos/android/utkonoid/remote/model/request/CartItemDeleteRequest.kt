package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartItemDeleteRequest(
    @SerializedName("GoodsItemId") var GoodsItemId:String,
    @SerializedName("Source")  var Source:String,
    @SerializedName("SourceId")  var SourceId:String,
    @SerializedName("Return")  var Return:CartItemDeleteReturn
)

data class CartItemDeleteReturn(
    @SerializedName("Cart") var Cart: Boolean,
    @SerializedName("CartItemList") var CartItemList:Boolean,
    @SerializedName("TotalCost") var TotalCost:Boolean
)