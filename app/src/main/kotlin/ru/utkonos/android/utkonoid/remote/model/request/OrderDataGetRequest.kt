package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class OrderDataGetRequest(
     @SerializedName("SupportAndroidPay") var SupportAndroidPay:Boolean,
     @SerializedName("SupportApplePay") var SupportApplePay:Boolean,
     @SerializedName("SupportSamsungPay") var SupportSamsungPay:Boolean,
     @SerializedName("SupportGooglePay") var SupportGooglePay:Boolean,
     @SerializedName("Return") var Return:OrderDataGetReturn
)

data class OrderDataGetReturn(
     @SerializedName("UserAddressList") var UserAddressList: Boolean,
     @SerializedName("LastReservations") var LastReservations: Boolean,
     @SerializedName("DeliveryIntervalList") var DeliveryIntervalList: Boolean,
     @SerializedName("FavoriteTerminalList") var FavoriteTerminalList: Boolean,
     @SerializedName("DeliveryInfo") var DeliveryInfo: Boolean,
     @SerializedName("OrderDeliveryOffer") var OrderDeliveryOffer: Boolean,
     @SerializedName("Payments") var Payments: Boolean,
     @SerializedName("User") var User: Boolean
)