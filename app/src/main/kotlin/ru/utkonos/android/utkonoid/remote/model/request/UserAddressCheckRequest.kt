package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressCheckRequest(
    @SerializedName("Latitude") var Latitude:String,
    @SerializedName("Longitude") var Longitude: String
)
