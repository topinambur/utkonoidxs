package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 *  FAILURE	Отказ
    CANCELED	Отменен
    WAITING	Оформляется
    ACCEPTED	Оформлен
    PRELIMINARY	Предварительный
    BUILD	Сборка заказа
    DELIVERING	В пути
    DELIVERED	Доставлен
    FOODBOX_DROPPED	Доставлен в точку выдачи
    PHARMACY_DROPPED	Доставлен в аптеку
 */
data class OrderStatus(
    @SerializedName("Code") var code: String,
    @SerializedName("Text") var text:String
)