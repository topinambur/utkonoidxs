package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class CartLookupRequest(
    @SerializedName("Source") var source : String,
    @SerializedName("Return") var Return : CartLookupReturn
)

data class CartLookupReturn(
    @SerializedName("AllowRemainsQuantity") var AllowRemainsQuantity: Boolean = true,
    @SerializedName("Goods") var Goods: Boolean = true,
    @SerializedName("GoodsForgottenList") var GoodsForgottenList: Boolean = true,
    @SerializedName("GoodsRecommendationList") var GoodsRecommendationList: Boolean = true,
    @SerializedName("OrderDeliveryOffer") var OrderDeliveryOffer: Boolean = true,
    @SerializedName("EditableOrderList") var EditableOrderList: Boolean = true,
    @SerializedName("StarsGoodsInBasket") var StarsGoodsInBasket: Boolean = true
)