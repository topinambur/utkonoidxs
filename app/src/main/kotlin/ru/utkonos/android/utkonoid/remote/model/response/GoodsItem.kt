package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Материал (товар)
 */
data class GoodsItem(
        /**
         * Штрих-код
         */
        var Barcode: String,

        /**
         * Поставщик
         */
        var Brand : String,

        /**
         * Остаток на складе
         */
        var Count: Int,

        /**
         * Описание (устаревшее), следует использовать GoodsDescriptionList
         */
        var Description: String,

        /**
         * Идентификатор раздела избранного
         */
        var FavoriteCategoryId: Int,

        /**
         * ПК категории (может отсутствовать у старых материалов)
         */
        var GoodsCategoryId: Int,

        /**
         * Расширенное описание
         */
        var GoodsDescriptionList : ArrayList<GoodsDescription>,

        /**
         * Идентификатор
         */
        var Id: String,

        /**
         * Ссылка на малое изображение
         */
        var ImageSmallUrl: String,

        /**
         * Ссылка на среднее изображение
         */
        var ImageBigUrl: String,

        /**
         * Ссылка на большое изображение
         */
        var ImagePreviewUrl: String,

        /**
         * Ссылка на изображение высокого разрешения
         */
        var ImageHighUrl: String,

        /**
         * Ссылка на изображение
         */
        var ImageUrl : String,

        /**
         * Количество этого товара в корзине
         */
        var InCartCount: Int,

        /**
         * Название
         */
        var Name: String,

        /**
         * Страна производства
         */
        var Manufacturer: String,

        /**
         * Цена за продажную единицу
         */
        var Price: String,

        /**
         * Старая цена за единицу. На сайте отображается как перечеркнутая
         */
        var OldPrice: String,

        /**
         * Артикул материала
         */
        var OriginalId: String,

        /**
         * Большой шильдик промо-акции (на страницу товара)
         */
        var IconBig: String,

        /**
         * Малый шильдик промо-акции (для списка товаров)
         */
        var IconSmall: String,

        /**
         * Надпись на шильдике промо-акции.
         */
        var IconText: String,

        /**
         * Изображения материала
         */
        var Images: ArrayList<Image>,

        /**
         * Вес брутто
         */
        var BruttoWeight:String,

        /**
         * Вес нетто
         */
        var NetWeight: String,

        /**
         * Единица продажи
         */
        var SaleUnit: String,

        /**
         * Рейтинг
         */
        var Rating: Rating,

        /**
         * Единицы продажи
         */
        var GoodsUnitList: ArrayList<GoodsUnit>,

        /**
         * Идентификаторы товаров из списка "Рекомендуем купить"
         */
        var CrosslinksGoodsIds: ArrayList<String>,

        /**
         * Идентификаторы товаров из списка "С этим товаром покупают"
         */
        var SuggestsGoodsIds: ArrayList<String>,

        /**
         * Идентификатор родительской категории верхнего уровня
         */
        var RootCategoryId: Int,

        /**
         * Имя родительской категории верхнего уровня
         */
        var RootCategoryName: String,


        /**
         * Идентификаторы товаров из списка "Товары-заменители" (он же "Похожие товары")
         */
        var AlternativesGoodsIds: ArrayList<String>,


        var SubscribeEmail: Boolean,
        var SubscribeSMS: Boolean,
        var Ads: String,

        @SerializedName("Preorder") var preorder: Boolean = false,

        @SerializedName("PreorderDate") var preorderDeliveryDate: Date,

        @SerializedName("PreorderCount") var PreorderCount: String,

        @SerializedName("PreorderId") var PreorderId: String,

        var StarPrice: String,
        var StarsCountPurchased:String,
        var StarsCountInBasket: String,
        var MinSaleQuantity: String = "1",
        var MaxSaleQuantity: String = "0",
        var ModelId: String,
        var SizeWidth: String,
        var SizeHeight: String,
        var SizeLength: String

)