package ru.utkonos.android.utkonoid.presentation.main

import ru.utkonos.android.utkonoid.presentation.BasePresenter
import ru.utkonos.android.utkonoid.presentation.BaseView
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse

interface MainFragmentContract {

    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun refreshMainPage(data: MainpageGetResponse)

        fun showErrorState()

        fun hideErrorState()

        fun showEmptyState()

        fun hideEmptyState()

    }

    interface Presenter : BasePresenter {
        fun retrieveMainPage()
    }

}