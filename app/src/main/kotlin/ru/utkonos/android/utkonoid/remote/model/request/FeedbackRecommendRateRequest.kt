package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class FeedbackRecommendRateRequest(
   @SerializedName("OrderId") var OrderId:String,
   @SerializedName("Rating") var Rating:String
)