package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class AreaSearchResponse(
    @SerializedName("AreaList") var AreaList : List<Area>
) : BaseResponse()
