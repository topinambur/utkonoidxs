package ru.utkonos.android.utkonoid.domain.interactor.main

import io.reactivex.Single
import ru.utkonos.android.utkonoid.data.UtkonosDataRepository
import ru.utkonos.android.utkonoid.domain.executor.PostExecutionThread
import ru.utkonos.android.utkonoid.domain.executor.ThreadExecutor
import ru.utkonos.android.utkonoid.domain.interactor.SingleUseCase
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import javax.inject.Inject

/**
 * Use case used for retreiving a [Object] of [MainpageGetResponse] instances from the [UtkonosDataRepository]
 */
open class MainPageGet @Inject constructor(val utkonosRepository: UtkonosDataRepository,
                                           threadExecutor: ThreadExecutor,
                                           postExecutionThread: PostExecutionThread):
        SingleUseCase<MainpageGetResponse, Void?>(threadExecutor, postExecutionThread) {

    public override fun buildUseCaseObservable(params: Void?): Single<MainpageGetResponse> {
        return utkonosRepository.getMainPage(MainpageGetRequest())
    }

}