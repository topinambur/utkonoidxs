package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

data class CartNotices(
    /**
     * Возможность начала оформления заказа
     */
    @SerializedName("AllowStart") var AllowStart: Int,

    /**
     * Возможность оформления заказа
     */
    @SerializedName("AllowOrder") var AllowOrder: Int,

    /**
     * Доступность подтверждения заказа
     */
    @SerializedName("AllowCheck") var AllowCheck: Int,

    /**
     * Сообщения корзины
     */
    @SerializedName("CartNoticeList") var CartNoticeList :ArrayList<CartNoticeList>
)