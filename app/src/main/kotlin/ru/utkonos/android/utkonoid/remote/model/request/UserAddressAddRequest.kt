package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class UserAddressAddRequest(
    @SerializedName("UserAddress") var UserAddress: UserAddressData
)

