package ru.utkonos.android.utkonoid.remote.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Ilya Solovyov on 04.06.2018.
 * is3k@ya.ru
 */
class OtherParams(@SerializedName("AppsFlyerEnabled") val appsFlyerEnabled: Boolean)
