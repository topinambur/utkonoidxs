package ru.utkonos.android.utkonoid.remote

import io.reactivex.Single
import ru.utkonos.android.utkonoid.data.repository.UtkonosRemote
import ru.utkonos.android.utkonoid.remote.converterfactory.WrappedRequest
import ru.utkonos.android.utkonoid.remote.model.request.MainpageGetRequest
import ru.utkonos.android.utkonoid.remote.model.request.UtkonosHeadRequest
import ru.utkonos.android.utkonoid.remote.model.response.MainpageGetResponse
import javax.inject.Inject

class UtkonosRemoteImpl @Inject constructor(
    private val utkonosService: UtkonosService,
    private val token:String,
    private val marketingKey:String,
    private val deviceId:String
) : UtkonosRemote {

    override fun getMainPage(body: MainpageGetRequest): Single<MainpageGetResponse> {
        return utkonosService.MainpageGet(WrappedRequest( UtkonosHeadRequest(sessionToken = token, marketingPartnerKey = marketingKey, deviceId = deviceId, method = "MainpageGet"), body))
    }

}