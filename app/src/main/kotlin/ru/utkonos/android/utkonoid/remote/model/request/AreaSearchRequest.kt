package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class AreaSearchRequest(
   @SerializedName("PurposeId") var PurposeId:String
)