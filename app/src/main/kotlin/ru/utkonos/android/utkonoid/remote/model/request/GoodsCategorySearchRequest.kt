package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class GoodsCategorySearchRequest(
   @SerializedName("CatalogueTypeId") var CatalogueTypeId:String,
   @SerializedName("Deep") var Deep:String,
   @SerializedName("Id") var Id:String,
   @SerializedName("ParentId") var ParentId:String,
   @SerializedName("Return") var Return: GoodsCategorySearchReturn
)

data class GoodsCategorySearchReturn(
    @SerializedName("Banners") var Banners:Boolean = false,
    @SerializedName("Havetime") var Havetime:Boolean = false,
    @SerializedName("Youknow") var Youknow:Boolean = false,
    @SerializedName("Redirects") var Redirects:Boolean = false
)