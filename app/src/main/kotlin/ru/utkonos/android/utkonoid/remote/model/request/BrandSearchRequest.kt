package ru.utkonos.android.utkonoid.remote.model.request

import com.google.gson.annotations.SerializedName

data class BrandSearchRequest(
        @SerializedName("Count") var Count:String,
        @SerializedName("Name") var Name:String,
        @SerializedName("Offset") var Offset:String

)